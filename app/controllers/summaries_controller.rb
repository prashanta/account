class SummariesController < ApplicationController
  before_action :set_summary, only: [:edit, :update]

  def index
    @summary = Summary.first
    @date_from = if @summary.nil? then Plutus::Transaction.first.transaction_date else Summary.first.date_from end
    @date_to = Date.today
    @assets_account = Plutus::Account.where(:type=>'Plutus::Asset').all
    @expense_account = Plutus::Account.where(:type=>'Plutus::Expense').all
    @liability_account = Plutus::Account.where(:type=>'Plutus::Liability').all
    @equity_account = Plutus::Account.where(:type=>'Plutus::Equity').all
    @income_account = Plutus::Account.where(:type=>'Plutus::Revenue').all    
  end

  def edit
  end

  def update
    respond_to do |format|
      if @summary.update(summary_params)
        format.html { redirect_to summaries_path }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @summary.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_summary
      @summary = Summary.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def summary_params
      params.require(:summary).permit(:date_from)
    end
end
