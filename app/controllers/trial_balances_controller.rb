class TrialBalancesController < ApplicationController
  before_action :set_trial_balance, only: [:show, :edit, :update, :destroy]

  # GET /trial_balances
  # GET /trial_balances.json
  def index
    @trial_balances = TrialBalance.all
  end

  # GET /trial_balances/1
  # GET /trial_balances/1.json
  def show
    assets_account = Plutus::Account.where(:type=>'Plutus::Asset').map {|t| t.id}.join(', ')
    expense_account = Plutus::Account.where(:type=>'Plutus::Expense').map {|t| t.id}.join(', ')
    liability_account = Plutus::Account.where(:type=>'Plutus::Liability').map {|t| t.id}.join(', ')
    equity_account = Plutus::Account.where(:type=>'Plutus::Equity').map {|t| t.id}.join(', ')
    income_account = Plutus::Account.where(:type=>'Plutus::Revenue').map {|t| t.id}.join(', ')
    @period_transactions = Plutus::Transaction.where('transaction_date >= ?', @trial_balance.from).where('transaction_date <= ?', @trial_balance.to).select(:id)
    transaction_id_string = @period_transactions.map {|t| t.id}.join(', ')
    @assets_period_amounts = Plutus::Amount.where("account_id IN (#{assets_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @expense_period_amounts = Plutus::Amount.where("account_id IN (#{expense_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @liability_period_amounts = Plutus::Amount.where("account_id IN (#{liability_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @equity_period_amounts = Plutus::Amount.where("account_id IN (#{equity_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @income_period_amounts = Plutus::Amount.where("account_id IN (#{income_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
  end

  # GET /trial_balances/new
  def new
    @trial_balance = TrialBalance.new
  end

  # GET /trial_balances/1/edit
  def edit
  end

  # POST /trial_balances
  # POST /trial_balances.json
  def create
    @trial_balance = TrialBalance.new(trial_balance_params)

    respond_to do |format|
      if @trial_balance.save
        format.html { redirect_to @trial_balance}
        format.json { render action: 'show', status: :created, location: @trial_balance }
      else
        format.html { render action: 'new' }
        format.json { render json: @trial_balance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trial_balances/1
  # PATCH/PUT /trial_balances/1.json
  def update
    respond_to do |format|
      if @trial_balance.update(trial_balance_params)
        format.html { redirect_to @trial_balance }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @trial_balance.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trial_balance
      @trial_balance = TrialBalance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trial_balance_params
      params.require(:trial_balance).permit(:from, :to)
    end
end
