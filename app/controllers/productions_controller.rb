class ProductionsController < ApplicationController
  before_action :set_production, only: [:show, :edit, :update]
  helper_method :sort_column, :sort_direction
  # GET /productions
  # GET /productions.json
  def index
    @productions = Production.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page => params[:page], :per_page => 30 )
  end

  # GET /productions/1
  # GET /productions/1.json
  def show
  end

  # GET /productions/new
  def new
    @production = Production.new
    1.times do
      product_items = @production.product_items.build
      product_raw_items = @production.product_raw_items.build
    end
  end

  # GET /productions/1/edit
  def edit
    if @production.approved?
      redirect_to productions_path,notice: 'Production Note is locked'
    end
  end

  # POST /productions
  # POST /productions.json
  def create
    @production = Production.new(production_params)

    respond_to do |format|
      if @production.save
        format.html { redirect_to @production }
        format.json { render action: 'show', status: :created, location: @production }
      else
        format.html { render action: 'new' }
        format.json { render json: @production.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /productions/1
  # PATCH/PUT /productions/1.json
  def update
    respond_to do |format|
      if @production.update(production_params)
        format.html { redirect_to @production}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @production.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /productions/1
  # DELETE /productions/1.json
  def approved 
    Production.transaction do
      @production = Production.find(params[:id])
      total_issue_amount = BigDecimal.new('0')
      @production.product_raw_items.each do |issue_item|
        item = Item.find(issue_item.item.id)
        issue_quantity = issue_item.qty
          item.inventory_ins.where("in_stock > 0").order("in_date").each do |inventory_item|
             unless issue_quantity.to_f <= 0
              if inventory_item.in_stock.to_f >= issue_quantity
                inventory_out = InventoryOut.new(:item_id=>item.id,:quantity=>issue_quantity.to_f,:rate=>inventory_item.rate,:amount=>(issue_quantity.to_f*inventory_item.rate.to_f),:out_date=>@production.production_date,:commercial_document => @production)
                inventory_out.save
                inventory_item.update(:in_stock=>inventory_item.in_stock.to_f-issue_quantity.to_f)
                total_issue_amount += (issue_quantity.to_f*inventory_item.rate.to_f)
                issue_quantity = 0.0

              else 
                inventory_out = InventoryOut.new(:item_id=>item.id,:quantity=>inventory_item.in_stock,:rate=>inventory_item.rate,:amount=>(inventory_item.in_stock.to_f*inventory_item.rate.to_f),:out_date=>@production.production_date,:commercial_document => @production)
                inventory_out.save
                issue_quantity = issue_quantity - inventory_item.in_stock
                total_issue_amount += (inventory_item.in_stock.to_f*inventory_item.rate.to_f)
                inventory_item.update(:in_stock=>0.0)
              end
            end 
          end
        end

        rate = (total_issue_amount.to_f/@production.product_items.sum(:qty))
        @production.product_items.each do |product_item|
          item = Item.find(product_item.item.id)
          inventory = InventoryIn.new(:item_id=>item.id,:quantity=>product_item.qty,:rate=>rate,:amount=>(product_item.qty.to_f*rate.to_f),:in_stock=>product_item.qty,:in_date=>@production.production_date,:commercial_document => @production)
          inventory.save  
        end
        raise ActiveRecord::Rollback unless @production.update(:approved=>true)
    end
    redirect_to @production,:notice=>"#{@production.errors.first}"
  end

  private

  def sort_column
    Production.column_names.include?(params[:sort]) ? params[:sort] : "production_date"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_production
      @production = Production.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def production_params
      params.require(:production).permit(:batch_no, :production_date, :approved, :internal_information,:product_items_attributes=>[:id,:item_id,:qty,:description,:mrp,:_destroy],:product_raw_items_attributes=>[:id,:item_id,:qty,:description,:_destroy])
    end
end
