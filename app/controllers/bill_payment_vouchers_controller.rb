class BillPaymentVouchersController < ApplicationController
  before_action :set_bill_payment_voucher, only: [:edit, :update]

  # GET /bill_payment_vouchers/new
  def new
    session[:return_to] = request.referer
    @bill = Bill.find(params[:bill_id]) if params[:bill_id].present?
    @bill_payment_voucher = BillPaymentVoucher.new
    1.times do
      bill_payments = @bill_payment_voucher.bill_payments.build
    end
  end

  # GET /bill_payment_vouchers/1/edit
  def edit
    session[:return_to] = request.referer
    @bill = Bill.find(params[:bill_id])
  end
  def create
    BillPaymentVoucher.transaction do
    @bill_payment_voucher = BillPaymentVoucher.new(bill_payment_voucher_params)
      if @bill_payment_voucher.save
        @transaction = BillPaymentVoucher.make_transaction(@bill_payment_voucher)
        raise ActiveRecord::Rollback unless @transaction.save
      end
    end
     redirect_to session.delete(:return_to)
  end
  # PATCH/PUT /bill_payment_vouchers/1
  # PATCH/PUT /bill_payment_vouchers/1.json
  def update
      BillPaymentVoucher.transaction do
      if @bill_payment_voucher.update_attributes(bill_payment_voucher_params) 
        @transaction = BillPaymentVoucher.update_transaction(@bill_payment_voucher)
        raise ActiveRecord::Rollback unless @transaction.save
     end
    end
    redirect_to session.delete(:return_to) 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bill_payment_voucher
      @bill_payment_voucher = BillPaymentVoucher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bill_payment_voucher_params
      params.require(:bill_payment_voucher).permit(:reference_no, :payment_date, :payee, :account_id, :description,:bill_payments_attributes=>[:id,:bill_id,:amount,:_destroy])
    end
end
