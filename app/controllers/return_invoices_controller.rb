class ReturnInvoicesController < ApplicationController
  before_action :set_return_invoice, only: [:show, :edit, :update]

 

  def show
  end

  # GET /return_invoices/new
  def new
    @invoice = Invoice.find(params[:invoice_id])
    @return_invoice = ReturnInvoice.new
    1.times do
      return_invoice_items = @return_invoice.return_invoice_items.build
    end
  end


  def edit
    @invoice = Invoice.find(params[:invoice_id])
  end
 
  def create
    @invoice = Invoice.find(params[:return_invoice][:invoice_id]) 
    @return_invoice = ReturnInvoice.new(return_invoice_params)
    respond_to do |format|
      if @return_invoice.save
        format.html { redirect_to @return_invoice}
        format.json { render action: 'show', status: :created, location: @return_invoice }
      else
        format.html { render action: 'new' }
        format.json { render json: @return_invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @invoice = Invoice.find(@return_invoice.invoice.id)
    respond_to do |format|
      if @return_invoice.update(return_invoice_params)
        format.html { redirect_to @return_invoice }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @return_invoice.errors, status: :unprocessable_entity }
      end
    end
  end
  def transaction
    @return_invoice = ReturnInvoice.find(params[:id])
     ReturnInvoice.transaction do
      @transaction = ReturnInvoice.make_transaction(@return_invoice)
      if @transaction.save
        @return_invoice.return_invoice_items.each do |return_item|
          item = Item.find(return_item.invoice_item.item.id)
          inventory_out = @return_invoice.invoice.inventory_outs.find_by(item_id: item.id)
          inventory_in = InventoryIn.new(:item_id=>item.id,:quantity=>return_item.quantity,:rate=>inventory_out.rate,:amount=>(return_item.quantity.to_f*inventory_out.rate.to_f),:in_stock=>return_item.quantity,:in_date=>@return_invoice.return_date,:commercial_document => @return_invoice)
          inventory_in.save
          @errors = true if return_item.invoice_item.qty < return_item.invoice_item.return_invoice_items.sum(:quantity)
        end
        redirect_to @return_invoice.invoice
      else
        redirect_to @return_invoice.invoice,notice: "#{@transactions.errors.first}"
      end  
      raise ActiveRecord::Rollback if @errors
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_return_invoice
      @return_invoice = ReturnInvoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def return_invoice_params
      params.require(:return_invoice).permit(:invoice_id, :amount, :return_date, :reference_no, :description,:return_invoice_items_attributes=>[:id,:return_invoice_id,:invoice_item_id,:tax_code_id,:quantity,:rate,:tax_amount,:amount,:_destroy])
    end
end
