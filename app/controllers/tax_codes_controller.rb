class TaxCodesController < ApplicationController
  before_action :set_tax_code, only: [:edit, :update, ]

  # GET /tax_codes
  # GET /tax_codes.json
  def index
    @tax_codes = TaxCode.all
  end

  # GET /tax_codes/new
  def new
    @tax_code = TaxCode.new
  end

  # GET /tax_codes/1/edit
  def edit
  end

  # POST /tax_codes
  # POST /tax_codes.json
  def create
    @tax_code = TaxCode.new(tax_code_params)

    respond_to do |format|
      if @tax_code.save
        format.html { redirect_to tax_codes_path}
      else
        format.html { render action: 'new' }
        format.json { render json: @tax_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tax_codes/1
  # PATCH/PUT /tax_codes/1.json
  def update
    respond_to do |format|
      if @tax_code.update(tax_code_params)
        format.html { redirect_to tax_codes_path }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tax_code.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tax_code
      @tax_code = TaxCode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tax_code_params
      params.require(:tax_code).permit(:name, :rate)
    end
end
