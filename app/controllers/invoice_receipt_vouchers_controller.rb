class InvoiceReceiptVouchersController < ApplicationController
  before_action :set_invoice_receipt_voucher, only: [:edit, :update]

  # GET /invoice_receipt_vouchers/new
  def new
    session[:return_to] = request.referer
    @invoice = Invoice.find(params[:invoice_id]) if params[:invoice_id].present?
    @invoice_receipt_voucher = InvoiceReceiptVoucher.new
    1.times do
      invoice_receipts = @invoice_receipt_voucher.invoice_receipts.build
    end
  end

  # GET /invoice_receipt_vouchers/1/edit
  def edit
    session[:return_to] = request.referer
    @invoice = Invoice.find(params[:invoice_id])
  end

  def create
    InvoiceReceiptVoucher.transaction do
    @invoice_receipt_voucher = InvoiceReceiptVoucher.new(invoice_receipt_voucher_params)
      if @invoice_receipt_voucher.save
        @transaction = InvoiceReceiptVoucher.make_transaction(@invoice_receipt_voucher)
        raise ActiveRecord::Rollback unless @transaction.save
      end
    end
     redirect_to session.delete(:return_to)
  end
  # PATCH/PUT /invoice_receipt_vouchers/1
  # PATCH/PUT /invoice_receipt_vouchers/1.json
  def update
      InvoiceReceiptVoucher.transaction do
      if @invoice_receipt_voucher.update_attributes(invoice_receipt_voucher_params) 
        @transaction = InvoiceReceiptVoucher.update_transaction(@invoice_receipt_voucher)
        raise ActiveRecord::Rollback unless @transaction.save
     end
    end
    redirect_to session.delete(:return_to) 
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice_receipt_voucher
      @invoice_receipt_voucher = InvoiceReceiptVoucher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_receipt_voucher_params
      params.require(:invoice_receipt_voucher).permit(:reference_no, :payment_date, :payer, :account_id, :description,:invoice_receipts_attributes=>[:id,:invoice_id,:amount,:_destroy])
    end
end
