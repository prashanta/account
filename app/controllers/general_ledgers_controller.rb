class GeneralLedgersController < ApplicationController
  before_action :set_general_ledger, only: [:show, :edit, :update, :destroy]

  # GET /general_ledgers
  # GET /general_ledgers.json
  def index
    @general_ledgers = GeneralLedger.all
  end

  # GET /general_ledgers/1
  # GET /general_ledgers/1.json
  def show
    assets_account = Plutus::Account.where(:type=>'Plutus::Asset').map {|t| t.id}.join(', ')
    expense_account = Plutus::Account.where(:type=>'Plutus::Expense').map {|t| t.id}.join(', ')
    liability_account = Plutus::Account.where(:type=>'Plutus::Liability').map {|t| t.id}.join(', ')
    equity_account = Plutus::Account.where(:type=>'Plutus::Equity').map {|t| t.id}.join(', ')
    income_account = Plutus::Account.where(:type=>'Plutus::Revenue').map {|t| t.id}.join(', ')
    @period_transactions = Plutus::Transaction.where('transaction_date >= ?', @general_ledger.from).where('transaction_date <= ?', @general_ledger.to).select(:id)
    transaction_id_string = @period_transactions.map {|t| t.id}.join(', ')
    @assets_period_amounts = Plutus::Amount.where("account_id IN (#{assets_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @expense_period_amounts = Plutus::Amount.where("account_id IN (#{expense_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @liability_period_amounts = Plutus::Amount.where("account_id IN (#{liability_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @equity_period_amounts = Plutus::Amount.where("account_id IN (#{equity_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @income_period_amounts = Plutus::Amount.where("account_id IN (#{income_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
  end

  # GET /general_ledgers/new
  def new
    @general_ledger = GeneralLedger.new
  end

  # GET /general_ledgers/1/edit
  def edit
  end

  # POST /general_ledgers
  # POST /general_ledgers.json
  def create
    @general_ledger = GeneralLedger.new(general_ledger_params)

    respond_to do |format|
      if @general_ledger.save
        format.html { redirect_to @general_ledger}
        format.json { render action: 'show', status: :created, location: @general_ledger }
      else
        format.html { render action: 'new' }
        format.json { render json: @general_ledger.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /general_ledgers/1
  # PATCH/PUT /general_ledgers/1.json
  def update
    respond_to do |format|
      if @general_ledger.update(general_ledger_params)
        format.html { redirect_to @general_ledger}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @general_ledger.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_general_ledger
      @general_ledger = GeneralLedger.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def general_ledger_params
      params.require(:general_ledger).permit(:from, :to)
    end
end
