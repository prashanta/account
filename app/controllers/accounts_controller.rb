class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update]
  def index
  @assets_account = Plutus::Account.where(:type=>'Plutus::Asset').all
  @expense_account = Plutus::Account.where(:type=>'Plutus::Expense').all
  @liability_account = Plutus::Account.where(:type=>'Plutus::Liability').all
  @equity_account = Plutus::Account.where(:type=>'Plutus::Equity').all
  @income_account = Plutus::Account.where(:type=>'Plutus::Revenue').all
  end

  def new
    @type = params[:type]
    @account = Plutus::Account.new
  end

  def create
    @account = Plutus::Account.new(account_params)
    if @account.save
      redirect_to accounts_path
    end
  end
  def show
    @general_ledger = GeneralLedger.find(params[:general_ledger_id]) if !params[:general_ledger_id].blank?
    @period_transactions   = Plutus::Transaction.where('transaction_date >= ?',params[:from]).where('transaction_date <= ?', params[:to]).select(:id)
    transaction_id_string  = @period_transactions.map {|t| t.id}.join(', ')
    @debit_amounts         = @account.debit_amounts.where("transaction_id IN (#{transaction_id_string})")
    @credit_amounts        = @account.credit_amounts.where("transaction_id IN (#{transaction_id_string})")
  end
  def edit
   redirect_to accounts_path,notice: 'Account is not Editable' if !@account.is_editable?
  end

  def update
    if @account.update(account_params)
      redirect_to accounts_path
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Plutus::Account.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:name, :type,:contra,:is_editable)
    end
end
