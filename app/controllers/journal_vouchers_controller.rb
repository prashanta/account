class JournalVouchersController < ApplicationController
  before_action :set_journal_voucher, only: [:edit, :update]

  # GET /journal_vouchers
  # GET /journal_vouchers.json
  def index
    @journal_vouchers = JournalVoucher.search(params[:search]).order("voucher_date Desc").paginate(:page => params[:page], :per_page => 30 )
  end

  # GET /journal_vouchers/new
  def new
    @journal_voucher = JournalVoucher.new
    1.times do
      debit_journals  =  @journal_voucher.debit_journals.build
      credit_journals =  @journal_voucher.credit_journals.build
    end
  end

  # GET /journal_vouchers/1/edit
  def edit
  end

  def create
    JournalVoucher.transaction do
    @journal_voucher = JournalVoucher.new(journal_voucher_params)
      if @journal_voucher.save
        @transaction = JournalVoucher.make_transaction(@journal_voucher)
        raise ActiveRecord::Rollback unless @transaction.save
      end
    end
     redirect_to journal_vouchers_path
  end
  # PATCH/PUT /journal_vouchers/1
  # PATCH/PUT /journal_vouchers/1.json
  def update
    JournalVoucher.transaction do
      if @journal_voucher.update_attributes(journal_voucher_params) 
        @transaction = JournalVoucher.update_transaction(@journal_voucher)
        raise ActiveRecord::Rollback unless @transaction.save
      end
    end
    redirect_to journal_vouchers_path
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_journal_voucher
      @journal_voucher = JournalVoucher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def journal_voucher_params
      params.require(:journal_voucher).permit(:voucher_date, :reference_no, :narration, :notes,:debit_journals_attributes=>[:id,:description,:account_id,:amount,:_destroy],:credit_journals_attributes=>[:id,:description,:account_id,:amount,:_destroy])
    end
end
