class GeneralPaymentVouchersController < ApplicationController
  before_action :set_general_payment_voucher, only: [:edit, :update, :destroy]

  # GET /general_payment_vouchers
  # GET /general_payment_vouchers.json
  def index
    @general_payment_vouchers = GeneralPaymentVoucher.search(params[:search]).order("payment_date Desc").paginate(:page => params[:page], :per_page => 30 )
  end


  # GET /general_payment_vouchers/new
  def new
    @general_payment_voucher = GeneralPaymentVoucher.new
    1.times do
      general_payments = @general_payment_voucher.general_payments.build
    end
  end

  # GET /general_payment_vouchers/1/edit
  def edit
  end
  def create
    GeneralPaymentVoucher.transaction do
    @general_payment_voucher = GeneralPaymentVoucher.new(general_payment_voucher_params)
      if @general_payment_voucher.save
        @transaction = GeneralPaymentVoucher.make_transaction(@general_payment_voucher)
        raise ActiveRecord::Rollback unless @transaction.save
      end
    end
     redirect_to general_payment_vouchers_path
  end
  # PATCH/PUT /general_payment_vouchers/1
  # PATCH/PUT /general_payment_vouchers/1.json
 def update
    GeneralPaymentVoucher.transaction do
    if @general_payment_voucher.update_attributes(general_payment_voucher_params) 
      @transaction = GeneralPaymentVoucher.update_transaction(@general_payment_voucher)
      raise ActiveRecord::Rollback unless @transaction.save
   end
  end
  redirect_to general_payment_vouchers_path 
end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_general_payment_voucher
      @general_payment_voucher = GeneralPaymentVoucher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def general_payment_voucher_params
      params.require(:general_payment_voucher).permit(:reference_no, :payment_date, :payee, :account_id, :description,:general_payments_attributes=>[:id,:account_id,:amount,:tax_code_id,:tax_amount,:_destroy])
    end
end
