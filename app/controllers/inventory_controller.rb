class InventoryController < ApplicationController
  def index
  	@items = Item.includes(:inventory_ins).where("items.item_type= ?",'ca').where('inventory_ins.in_stock > 0').order('name')
  end
end
