class GeneralReceiptVouchersController < ApplicationController
  before_action :set_general_receipt_voucher, only: [:edit, :update]

  # GET /general_receipt_vouchers
  # GET /general_receipt_vouchers.json
  def index
    @general_receipt_vouchers = GeneralReceiptVoucher.search(params[:search]).order("payment_date Desc").paginate(:page => params[:page], :per_page => 30 )
  end


  # GET /general_receipt_vouchers/new
  def new
    @general_receipt_voucher = GeneralReceiptVoucher.new
    1.times do
      general_receipts = @general_receipt_voucher.general_receipts.build
    end
  end

  # GET /general_receipt_vouchers/1/edit
  def edit
  end

  def create
    GeneralReceiptVoucher.transaction do
    @general_receipt_voucher = GeneralReceiptVoucher.new(general_receipt_voucher_params)
      if @general_receipt_voucher.save
        @transaction = GeneralReceiptVoucher.make_transaction(@general_receipt_voucher) 
        raise ActiveRecord::Rollback unless @transaction.save
      end
    end
     redirect_to general_receipt_vouchers_path 
  end

  # PATCH/PUT /general_receipt_vouchers/1
  # PATCH/PUT /general_receipt_vouchers/1.json
 def update
    GeneralReceiptVoucher.transaction do
    if @general_receipt_voucher.update_attributes(general_receipt_voucher_params) 
      @transaction = GeneralReceiptVoucher.update_transaction(@general_receipt_voucher)
      raise ActiveRecord::Rollback unless @transaction.save
   end
  end
  redirect_to general_receipt_vouchers_path 
end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_general_receipt_voucher
      @general_receipt_voucher = GeneralReceiptVoucher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def general_receipt_voucher_params
      params.require(:general_receipt_voucher).permit(:reference_no, :payment_date, :payer, :account_id, :description,:general_receipts_attributes=>[:id,:account_id,:amount,:tax_code_id,:tax_amount,:_destroy])
    end
end
