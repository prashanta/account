class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]

  # GET /invoices
  # GET /invoices.json
  def index
    @invoices = Invoice.search(params[:search]).order("issue_date Desc").paginate(:page => params[:page], :per_page => 30 )
  end

  # GET /invoices/1
  # GET /invoices/1.json
  def show
  end

  # GET /invoices/new
  def new
    @invoice = Invoice.new
    1.times do
      invoice_items = @invoice.invoice_items.build
    end
  end

  # GET /invoices/1/edit
  def edit
    if @invoice.transactions.count >0
      redirect_to @invoice,notice: 'Invoice is locked'
    end
  end
 def create
    @invoice = Invoice.new(invoice_params)
     respond_to do |format|
      if @invoice.save
        format.html { redirect_to @invoice}
        format.json { render action: 'show', status: :created, location: @invoice }
      else
        format.html { render action: 'new' }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /invoices/1
  # PATCH/PUT /invoices/1.json
  def update
    respond_to do |format|
      if @invoice.update_attributes(invoice_params) 
        format.html { redirect_to @invoice}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def transaction
    Invoice.transaction do
    @invoice = Invoice.find(params[:id])
        @transaction = Invoice.make_transaction(@invoice)
          @invoice.invoice_items.each do |invoice_item|
          item = Item.find(invoice_item.item.id)
          issue_quantity = invoice_item.qty
          item.inventory_ins.where("in_stock > 0").order("in_date").each do |inventory_item|
            unless issue_quantity.to_f <= 0
              if inventory_item.in_stock.to_f > issue_quantity
                inventory_out = InventoryOut.new(:item_id=>item.id,:quantity=>issue_quantity.to_f,:rate=>inventory_item.rate,:amount=>(issue_quantity.to_f*inventory_item.rate.to_f),:out_date=>@invoice.issue_date,:commercial_document => @invoice)
                inventory_out.save
                inventory_item.update(:in_stock=>inventory_item.in_stock.to_f-issue_quantity.to_f)
                issue_quantity = 0.0
              else
                inventory_out = InventoryOut.new(:item_id=>item.id,:quantity=>inventory_item.in_stock,:rate=>inventory_item.rate,:amount=>(inventory_item.in_stock.to_f*inventory_item.rate.to_f),:out_date=>@invoice.issue_date,:commercial_document => @invoice)
                inventory_out.save
                issue_quantity = issue_quantity - inventory_item.in_stock
                inventory_item.update(:in_stock=>0.0)
              end
            end
          end
        end
        raise ActiveRecord::Rollback unless @transaction.save
      end
     redirect_to @invoice
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_params
      params.require(:invoice).permit(:invoice_no, :issue_date, :due_date, :customer_id, :internal_information, :amount, :amount_received,:invoice_items_attributes=>[:id,:item_id,:account_id,:qty,:price,:tax_code_id, :tax_amount,:amount,:_destroy])
    end
end
