class ProfitAndLossesController < ApplicationController
  before_action :set_profit_and_loss, only: [:show, :edit, :update, :destroy]

  # GET /profit_and_losses
  # GET /profit_and_losses.json
  def index
    @profit_and_losses = ProfitAndLoss.all
  end

  # GET /profit_and_losses/1
  # GET /profit_and_losses/1.json
  def show
    income_account = Plutus::Account.where(:type=>'Plutus::Revenue').map {|t| t.id}.join(', ')
    expense_account = Plutus::Account.where(:type=>'Plutus::Expense').map {|t| t.id}.join(', ')
    @period_transactions = Plutus::Transaction.where('transaction_date >= ?', @profit_and_loss.from).where('transaction_date <= ?', @profit_and_loss.to).select(:id)
    transaction_id_string = @period_transactions.map {|t| t.id}.join(', ')
    @income_period_amounts = Plutus::Amount.where("account_id IN (#{income_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @expense_period_amounts = Plutus::Amount.where("account_id IN (#{expense_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
  end

  # GET /profit_and_losses/new
  def new
    @profit_and_loss = ProfitAndLoss.new
  end

  # GET /profit_and_losses/1/edit
  def edit
  end

  # POST /profit_and_losses
  # POST /profit_and_losses.json
  def create
    @profit_and_loss = ProfitAndLoss.new(profit_and_loss_params)

    respond_to do |format|
      if @profit_and_loss.save
        format.html { redirect_to @profit_and_loss }
        format.json { render action: 'show', status: :created, location: @profit_and_loss }
      else
        format.html { render action: 'new' }
        format.json { render json: @profit_and_loss.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profit_and_losses/1
  # PATCH/PUT /profit_and_losses/1.json
  def update
    respond_to do |format|
      if @profit_and_loss.update(profit_and_loss_params)
        format.html { redirect_to @profit_and_loss}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @profit_and_loss.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profit_and_loss
      @profit_and_loss = ProfitAndLoss.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profit_and_loss_params
      params.require(:profit_and_loss).permit(:from, :to)
    end
end
