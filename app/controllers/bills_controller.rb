class BillsController < ApplicationController
  before_action :set_bill, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction
  # GET /bills
  # GET /bills.json
  def index
    @bills = Bill.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:page => params[:page], :per_page => 30 )
  end

  # GET /bills/1
  # GET /bills/1.json
  def show
  end

  # GET /bills/new
  def new
    @bill = Bill.new
    1.times do
      bill_items = @bill.bill_items.build
    end
  end

  # GET /bills/1/edit
  def edit
    if @bill.transactions.count >0
      redirect_to @bill,notice: 'Bill is locked'
    end
  end
  def create
    @bill = Bill.new(bill_params)
    respond_to do |format|
      if @bill.save
        format.html { redirect_to @bill}
        format.json { render action: 'show', status: :created, location: @bill }
      else
        format.html { render action: 'new' }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /bills/1
  # PATCH/PUT /bills/1.json
  def update
    respond_to do |format|
      if @bill.update_attributes(bill_params) 
        format.html { redirect_to @bill}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end

  def transaction
    @bill = Bill.find(params[:id])
      @transaction = Bill.make_transaction(@bill)
      if @transaction.save
        @bill.bill_items.each do |bill_item|
          item = Item.find(bill_item.item.id)
          rate = if bill_item.tax_code.nil? then bill_item.price else bill_item.price-((bill_item.price*bill_item.tax_code.rate)/(100+bill_item.tax_code.rate)) end
          inventory = InventoryIn.new(:item_id=>item.id,:quantity=>bill_item.qty,:rate=>rate,:amount=>(bill_item.qty.to_f*rate.to_f),:in_stock=>bill_item.qty,:in_date=>@bill.issue_date,:commercial_document => @bill)
          inventory.save
        end
        redirect_to @bill
      else
        redirect_to @bill,notice: "#{@transaction.errors.first}"
      end  
  end
  private

  def sort_column
    Bill.column_names.include?(params[:sort]) ? params[:sort] : "issue_date"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_bill
      @bill = Bill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bill_params
      params.require(:bill).permit(:issue_date, :due_date, :supplier_id, :reference_no, :internal_information, :amount, :amount_paid,:bill_items_attributes=>[:id,:account_id,:item_id,:qty,:price,:tax_code_id, :tax_amount,:amount,:_destroy])
    end
end
