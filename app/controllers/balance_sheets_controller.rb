class BalanceSheetsController < ApplicationController
  before_action :set_balance_sheet, only: [:show, :edit, :update, :destroy]

  # GET /balance_sheets
  # GET /balance_sheets.json
  def index
    @balance_sheets = BalanceSheet.all
  end

  # GET /balance_sheets/1
  # GET /balance_sheets/1.json
  def show
    assets_account = Plutus::Account.where(:type=>'Plutus::Asset').map {|t| t.id}.join(', ')
    liability_account = Plutus::Account.where(:type=>'Plutus::Liability').map {|t| t.id}.join(', ')
    equity_account = Plutus::Account.where(:type=>'Plutus::Equity').map {|t| t.id}.join(', ')
    @period_transactions = Plutus::Transaction.where('transaction_date <= ?', @balance_sheet.as_at).select(:id)
    transaction_id_string = @period_transactions.map {|t| t.id}.join(', ')
    @assets_period_amounts = Plutus::Amount.where("account_id IN (#{assets_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @liability_period_amounts = Plutus::Amount.where("account_id IN (#{liability_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
    @equity_period_amounts = Plutus::Amount.where("account_id IN (#{equity_account})").where("transaction_id IN (#{transaction_id_string})").group(:account_id)
  end

  # GET /balance_sheets/new
  def new
    @balance_sheet = BalanceSheet.new
  end

  # GET /balance_sheets/1/edit
  def edit
  end

  # POST /balance_sheets
  # POST /balance_sheets.json
  def create
    @balance_sheet = BalanceSheet.new(balance_sheet_params)

    respond_to do |format|
      if @balance_sheet.save
        format.html { redirect_to @balance_sheet }
        format.json { render action: 'show', status: :created, location: @balance_sheet }
      else
        format.html { render action: 'new' }
        format.json { render json: @balance_sheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /balance_sheets/1
  # PATCH/PUT /balance_sheets/1.json
  def update
    respond_to do |format|
      if @balance_sheet.update(balance_sheet_params)
        format.html { redirect_to @balance_sheet}
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @balance_sheet.errors, status: :unprocessable_entity }
      end
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_balance_sheet
      @balance_sheet = BalanceSheet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def balance_sheet_params
      params.require(:balance_sheet).permit(:as_at)
    end
end
