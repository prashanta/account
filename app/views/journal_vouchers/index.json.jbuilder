json.array!(@journal_vouchers) do |journal_voucher|
  json.extract! journal_voucher, :voucher_date, :reference_no, :narration, :notes
  json.url journal_voucher_url(journal_voucher, format: :json)
end
