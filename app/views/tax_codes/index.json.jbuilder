json.array!(@tax_codes) do |tax_code|
  json.extract! tax_code, :name, :rate
  json.url tax_code_url(tax_code, format: :json)
end
