json.array!(@items) do |item|
  json.extract! item, :name, :unit, :description, :account_id
  json.url item_url(item, format: :json)
end
