json.array!(@general_ledgers) do |general_ledger|
  json.extract! general_ledger, :from, :to
  json.url general_ledger_url(general_ledger, format: :json)
end
