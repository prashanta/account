json.array!(@general_receipt_vouchers) do |general_receipt_voucher|
  json.extract! general_receipt_voucher, :reference_no, :payment_date, :payer, :account_id, :description
  json.url general_receipt_voucher_url(general_receipt_voucher, format: :json)
end
