class GeneralPaymentVoucher < ActiveRecord::Base
  belongs_to :account,:class_name => "Plutus::Account"
  has_one :transaction, :as => :commercial_document,:class_name => "Plutus::Transaction"
  has_many :general_payments,:dependent=>:destroy
  accepts_nested_attributes_for :general_payments, :reject_if => lambda { |a| a[:account_id].blank? or a[:amount].blank? }, :allow_destroy => true

    def self.debits(general_payment_voucher)
    Plutus::Account
    debits = []
    general_payment_voucher.general_payments.each do |payment|
      if payment.tax_code.blank?
      	debits << {:account=>payment.account_id,:amount=>payment.amount.round(2)}
      else
      	 @tax_account = Plutus::Liability.find_or_create_by(:name => payment.tax_code.name)
      	debits << {:account=>payment.account_id,:amount=>(payment.amount - payment.tax_amount.round(2)).round(2)}
      	debits << {:account=>@tax_account.id,:amount=>payment.tax_amount.round(2)}
      end
    end
    debits
   end
   def self.credits(general_payment_voucher)
     paid_amount = general_payment_voucher.general_payments.map {|s| s['amount'].to_f}.reduce(0, :+)
     Plutus::Account
     [{:account=>general_payment_voucher.account,:amount=>paid_amount.round(2)}]
   end
  def self.make_transaction(general_payment_voucher)
  	 debits = self.debits(general_payment_voucher)
  	 credits = self.credits(general_payment_voucher)
     transaction = Plutus::Transaction.build(:transaction_date => general_payment_voucher.payment_date,:description => "Being Payment to #{general_payment_voucher.payee}", :commercial_document => general_payment_voucher,
     :debits=>debits,:credits=>credits)
    transaction
  end
  def self.update_transaction(general_payment_voucher)
    prev_transaction = Plutus::Transaction.find(general_payment_voucher.transaction.id)
    prev_transaction.debit_amounts.delete_all
    prev_transaction.credit_amounts.delete_all
    prev_transaction.delete
     debits = self.debits(general_payment_voucher)
     credits = self.credits(general_payment_voucher)
     transaction = Plutus::Transaction.build(:transaction_date => general_payment_voucher.payment_date,:description => "Being Payment to #{general_payment_voucher.payee}", :commercial_document => general_payment_voucher,
     :debits=>debits,:credits=>credits)
     transaction
  end

  def self.search(search)
    if search
      begin
        where('payment_date = ?', search.to_date)
      rescue
        where('reference_no LIKE ?',"%#{search}%")
      end
    else
      all.order('payment_date DESC')
    end
  end  
end
