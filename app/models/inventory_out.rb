class InventoryOut < ActiveRecord::Base
  belongs_to :item
  belongs_to :commercial_document, :polymorphic => true
end
