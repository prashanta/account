class ProductItem < ActiveRecord::Base
  belongs_to :item
  belongs_to :production
  before_save :validate_fields
  protected

   def validate_fields
     self.qty = 1 if self.qty.blank?
   end
end
