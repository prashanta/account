class Customer < ActiveRecord::Base
	has_many :invoices,:dependent=>:destroy

  def self.search(search)
    if search
        where('name LIKE ?',"%#{search}%")
    else
      all.order('name')
    end
  end	
end
