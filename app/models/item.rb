class Item < ActiveRecord::Base
  has_many :invoice_items,:dependent=>:destroy
  has_many :bill_items,:dependent=>:destroy
  has_many :product_items,:dependent=>:destroy
  has_many :product_raw_items,:dependent=>:destroy
  has_many :inventory_ins,:dependent=>:destroy
  has_many :inventory_outs,:dependent=>:destroy
	validates_presence_of :name, :unit,:item_type
  before_save :item_upcase?
    def item_upcase?
      self.name=self.name.upcase if !self.name.blank?
      self.unit=self.unit.upcase if !self.unit.blank?
    end
	def inventory
		stock = self.inventory_ins.sum(:in_stock)
		return stock.round(2)
	end

  def inventory_in_qty
    inventory_in=self.inventory_ins.where('in_date <= ?',Date.today).sum(:quantity)
    return inventory_in.round(2)
  end
   def inventory_in_amount
    inventory_amount=self.inventory_ins.where('in_date <= ?',Date.today).sum(:amount)
    return inventory_amount.round(2)
  end
  def inventory_out_qty
    inventory_out=self.inventory_outs.where('out_date <= ?', Date.today).sum(:quantity)
    return inventory_out.round(2)
  end
   def inventory_out_amount
    inventory_out_amount=self.inventory_outs.where('out_date <= ?', Date.today).sum(:amount)
    return inventory_out_amount.round(2)
  end

  def inventory_stock_amount
    return inventory_in_amount - inventory_out_amount
  end
end
