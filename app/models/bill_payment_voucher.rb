class BillPaymentVoucher < ActiveRecord::Base
  belongs_to :account,:class_name => "Plutus::Account"
  has_one :transaction, :as => :commercial_document,:class_name => "Plutus::Transaction"
  has_many :bill_payments,:dependent=>:destroy
  accepts_nested_attributes_for :bill_payments, :reject_if => lambda { |a| a[:bill_id].blank? or a[:amount].blank? }, :allow_destroy => true

    def self.debits(bill_payment_voucher)
    paid_amount = bill_payment_voucher.bill_payments.map {|s| s['amount'].to_f}.reduce(0, :+)
    Plutus::Account
    @debit_account = Plutus::Liability.find_or_create_by(:name => "Account Payable")
    [{:account=>@debit_account.id,:amount=>paid_amount.round(2)}]
   end
   def self.credits(bill_payment_voucher)
     paid_amount = bill_payment_voucher.bill_payments.map {|s| s['amount'].to_f}.reduce(0, :+)
     Plutus::Account
     [{:account=>bill_payment_voucher.account.id,:amount=>paid_amount.round(2)}]
   end
  def self.make_transaction(bill_payment_voucher)
     transaction = Plutus::Transaction.build(:transaction_date => bill_payment_voucher.payment_date,:description => "Being Payment bill to #{bill_payment_voucher.payee}", :commercial_document => bill_payment_voucher,
     :debits=>self.debits(bill_payment_voucher),:credits=>self.credits(bill_payment_voucher))
    transaction
  end

  def self.update_transaction(bill_payment_voucher)
    prev_transaction = Plutus::Transaction.find(bill_payment_voucher.transaction.id)
    prev_transaction.debit_amounts.delete_all
    prev_transaction.credit_amounts.delete_all
    prev_transaction.delete
     transaction = Plutus::Transaction.build(:transaction_date => bill_payment_voucher.payment_date,:description => "Being Payment bill to #{bill_payment_voucher.payee}", :commercial_document => bill_payment_voucher,
     :debits=>self.debits(bill_payment_voucher),:credits=>self.credits(bill_payment_voucher))
     transaction
  end 
end
