class ReturnInvoiceItem < ActiveRecord::Base
  belongs_to :return_invoice
  belongs_to :invoice_item
  belongs_to :tax_code

  before_save :validate_fields
   protected

   def validate_fields
   	 if self.quantity.blank? or self.quantity == 0
   	 	quantity = 1 
   	 else
   	 	quantity = self.quantity
   	 end
   	 rate = self.invoice_item.price if !self.invoice_item.price.blank?
   	 amount= quantity.to_f * rate.to_f
     self.quantity = quantity.to_f
     self.rate = rate.to_f
     self.amount = amount.to_f
     self.tax_code_id = self.invoice_item.tax_code.id if !self.invoice_item.tax_code.nil?
     self.tax_amount = ((amount*self.invoice_item.tax_code.rate)/(100+self.invoice_item.tax_code.rate)).round(2) if !self.invoice_item.tax_code.nil?
   end
  
end
