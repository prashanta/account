class Invoice < ActiveRecord::Base
  belongs_to :customer
  has_many :invoice_items,:dependent=>:destroy
  has_many :invoice_receipts,:dependent=>:destroy
  has_many :transactions, :as => :commercial_document,:class_name => "Plutus::Transaction"
  has_many :inventory_outs, :as => :commercial_document
  has_many :return_invoices, :dependent=>:destroy
  before_save :validate_fields
  validates_presence_of :issue_date,:customer_id
  validates_uniqueness_of :invoice_no
  validate :item_quantity?
  accepts_nested_attributes_for :invoice_items, :reject_if => lambda { |a| a[:item_id].blank? or a[:account_id].blank? or a[:price].blank? }, :allow_destroy => true
  protected

   def validate_fields
     self.amount = 0 if self.amount.blank?
   end
   def self.debits(invoice)
    Plutus::Account
    @debit_account = Plutus::Asset.find_or_create_by(:name => "Account receivable")
    [{:account=>Plutus::Account.find(@debit_account.id),:amount=>(invoice.amount.round(2))}]
   end
   def self.credits(invoice)
    Plutus::Account
    credits = []
    invoice.invoice_items.each do |invoice_item|
      if invoice_item.tax_code.blank?
        credits << {:account=>Plutus::Account.find(invoice_item.account_id),:amount=>invoice_item.amount.round(2)}
      else
         @tax_account = Plutus::Liability.find_or_create_by(:name => invoice_item.tax_code.name)
        credits << {:account=>Plutus::Account.find(invoice_item.account_id),:amount=>(invoice_item.amount - invoice_item.tax_amount.round(2)).round(2)}
        credits << {:account=>Plutus::Account.find(@tax_account.id),:amount=>invoice_item.tax_amount.round(2)}
      end
    end
    credits
   end
    def self.make_transaction(invoice)
       transaction = Plutus::Transaction.build(:transaction_date => invoice.issue_date,:description => "Being Invoice Created", :commercial_document => invoice,
       :debits=>self.debits(invoice),:credits=>self.credits(invoice))
      transaction
    end
    def item_quantity?
      self.invoice_items.each do |invoice_item|
      item = Item.find(invoice_item.item.id)
      errors[:base] << "Item Quantity must be less then stock" if item.inventory_ins.sum(:in_stock) < invoice_item.qty
      end
    end 
    def self.search(search)
      if search
        begin
          where('issue_date = ?', search.to_date)
        rescue
          where('invoice_no LIKE ?',"%#{search}%")
        end
      else
        all.order('issue_date DESC')
      end
  end 
end