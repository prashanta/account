class Bill < ActiveRecord::Base
  belongs_to :supplier
  has_many :bill_items,:dependent=>:destroy
  has_many :bill_payments,:dependent=>:destroy
  has_many :transactions, :as => :commercial_document,:class_name => "Plutus::Transaction"
  has_many :inventory_ins, :as => :commercial_document
  before_save :validate_fields
  validates_presence_of :issue_date,:supplier_id
  validates_uniqueness_of :reference_no
  accepts_nested_attributes_for :bill_items, :reject_if => lambda { |a| a[:item_id].blank? or a[:account_id].blank? or a[:price].blank? }, :allow_destroy => true
 protected

   def validate_fields
     self.amount = 0 if self.amount.blank?
   end
   def self.debits(bill)
    Plutus::Account
    debits = []
    bill.bill_items.each do |bill_item|
      if bill_item.tax_code.blank?
        debits << {:account=>bill_item.account_id,:amount=>bill_item.amount.round(2)}
      else
        @tax_account = Plutus::Liability.find_or_create_by(:name => bill_item.tax_code.name)
        debits << {:account=>bill_item.account_id,:amount=>(bill_item.amount - bill_item.tax_amount.round(2)).round(2)}
        debits << {:account=>@tax_account.id,:amount=>bill_item.tax_amount.round(2)}
      end
    end
    debits
   end
   def self.credits(bill)
    Plutus::Account
    @credit_account = Plutus::Liability.find_or_create_by(:name => "Account Payable")
    [{:account=>@credit_account.id,:amount=>bill.amount.round(2)}]
   end
    def self.make_transaction(bill)
       transaction = Plutus::Transaction.build(:transaction_date => bill.issue_date,:description => "Being Bill Created", :commercial_document => bill,
       :debits=>self.debits(bill),:credits=>self.credits(bill))
      transaction
    end
  
  def self.search(search)
    if search
      begin
        where('issue_date = ?', search.to_date)
      rescue
        where('reference_no LIKE ?',"%#{search}%")
      end
    else
      all.order('issue_date DESC')
    end
  end


end
