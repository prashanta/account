class InvoiceReceiptVoucher < ActiveRecord::Base
  belongs_to :account,:class_name => "Plutus::Account"
  has_one :transaction, :as => :commercial_document,:class_name => "Plutus::Transaction"
  has_many :invoice_receipts,:dependent=>:destroy
  validates_presence_of :account_id
  accepts_nested_attributes_for :invoice_receipts, :reject_if => lambda { |a| a[:invoice_id].blank? or a[:amount].blank? }, :allow_destroy => true

   def self.debits(invoice_receipt_voucher)
     paid_amount = invoice_receipt_voucher.invoice_receipts.map {|s| s['amount'].to_f}.reduce(0, :+)
     Plutus::Account
     [{:account=>invoice_receipt_voucher.account.id,:amount=>paid_amount.round(2)}]
   end
    def self.credits(invoice_receipt_voucher)
    paid_amount = invoice_receipt_voucher.invoice_receipts.map {|s| s['amount'].to_f}.reduce(0, :+)
    Plutus::Account
    @debit_account = Plutus::Asset.find_or_create_by(:name => "Account receivable")
    [{:account=>@debit_account.id,:amount=>paid_amount.round(2)}]
   end
  def self.make_transaction(invoice_receipt_voucher)
     transaction = Plutus::Transaction.build(:transaction_date => invoice_receipt_voucher.payment_date,:description => "Being Amount Received on Invoice form #{invoice_receipt_voucher.payer}", :commercial_document => invoice_receipt_voucher,
     :debits=>self.debits(invoice_receipt_voucher),:credits=>self.credits(invoice_receipt_voucher))
    transaction
  end
  def self.update_transaction(invoice_receipt_voucher)
    prev_transaction = Plutus::Transaction.find(invoice_receipt_voucher.transaction.id)
    prev_transaction.debit_amounts.delete_all
    prev_transaction.credit_amounts.delete_all
    prev_transaction.delete
      transaction = Plutus::Transaction.build(:transaction_date => invoice_receipt_voucher.payment_date,:description => "Being Amount Received on Invoice form #{invoice_receipt_voucher.payer}", :commercial_document => invoice_receipt_voucher,
     :debits=>self.debits(invoice_receipt_voucher),:credits=>self.credits(invoice_receipt_voucher))
     transaction
  end
end
