class Production < ActiveRecord::Base
   has_many :product_items,:dependent=>:destroy
   has_many :product_raw_items,:dependent=>:destroy
   has_many :inventory_ins, :as => :commercial_document
   has_many :inventory_outs, :as => :commercial_document
   accepts_nested_attributes_for :product_items, :reject_if => lambda { |a| a[:item_id].blank? or a[:qty].blank? }, :allow_destroy => true
   accepts_nested_attributes_for :product_raw_items, :reject_if => lambda { |a| a[:item_id].blank? or a[:qty].blank? }, :allow_destroy => true
   validate :item_quantity?
   validates_uniqueness_of :batch_no
      def item_quantity?
        if !self.approved?
          self.product_raw_items.each do |raw_item|
          item = Item.find(raw_item.item.id)
          errors[:base] << "Item Quantity must be less then stock" if item.inventory_ins.sum(:in_stock) < raw_item.qty
        end
      end
    end 
    def self.search(search)
    if search
      begin
        where('production_date = ?', search.to_date)
      rescue
        where('batch_no LIKE ?',"%#{search}%")
      end
    else
      all.order('production_date DESC')
    end
  end
end
