class ReturnInvoice < ActiveRecord::Base
  belongs_to :invoice
  has_many :return_invoice_items,:dependent=>:destroy
  has_many :transactions, :as => :commercial_document,:class_name => "Plutus::Transaction"
  has_many :inventory_ins, :as => :commercial_document
  validates_uniqueness_of :reference_no
  accepts_nested_attributes_for :return_invoice_items, :reject_if => lambda { |a| a[:invoice_item_id].blank? or a[:quantity].blank?}, :allow_destroy => true
  validate :item_quantity?, on: :create
  validate :return_invoice_items?
  before_save :set_amount

  protected
   def item_quantity?
   	self.return_invoice_items.each do |return_item|
      invoice_item = InvoiceItem.find(return_item.invoice_item.id)
      if invoice_item.return_invoice_items.count >0
        quantity = return_item.quantity - invoice_item.return_invoice_items.sum(:quantity)
      else
        quantity = return_item.quantity
      end
      errors[:base] << "Item Quantity must be less then Sale" if quantity < return_item.quantity
    end
  end
  def return_invoice_items?
    errors[:base] << "Please select a item" if self.return_invoice_items.count < 0
  end
 
  def set_amount
    amount = BigDecimal.new('0')
    self.return_invoice_items.each do |return_item|
      if return_item.quantity.blank? or return_item.quantity == 0
        quantity = 1 
      else
        quantity = return_item.quantity
      end
       rate = return_item.invoice_item.price if !return_item.invoice_item.price.blank?
       amount += quantity.to_f * rate.to_f
    end
    self.amount = amount
  end
  def self.credits(return_invoice)
    Plutus::Account
    @account = Plutus::Asset.find_or_create_by(:name => "Account receivable")
    [{:account=>Plutus::Account.find(@account.id),:amount=>(return_invoice.amount.round(2))}]
   end
   def self.debits(return_invoice)
    Plutus::Account
    credits = []
    return_invoice.return_invoice_items.each do |return_invoice_item|
      if return_invoice_item.tax_code.blank?
        credits << {:account=>Plutus::Account.find(return_invoice_item.invoice_item.account_id),:amount=>return_invoice_item.amount.round(2)}
      else
         @tax_account = Plutus::Liability.find_or_create_by(:name => return_invoice_item.invoice_item.tax_code.name)
        credits << {:account=>Plutus::Account.find(return_invoice_item.invoice_item.account_id),:amount=>(return_invoice_item.amount - return_invoice_item.tax_amount.round(2)).round(2)}
        credits << {:account=>Plutus::Account.find(@tax_account.id),:amount=>return_invoice_item.tax_amount.round(2)}
      end
    end
    credits
   end
    def self.make_transaction(return_invoice)
       transaction = Plutus::Transaction.build(:transaction_date => return_invoice.return_date,:description => "Being Invoice #{return_invoice.invoice.invoice_no} Return", :commercial_document => return_invoice,
       :debits=>self.debits(return_invoice),:credits=>self.credits(return_invoice))
      transaction
    end
end
