class GeneralReceiptVoucher < ActiveRecord::Base
  belongs_to :account,:class_name => "Plutus::Account"
  has_many :general_receipts,:dependent=>:destroy
  has_one :transaction, :as => :commercial_document,:class_name => "Plutus::Transaction"
  accepts_nested_attributes_for :general_receipts, :reject_if => lambda { |a| a[:account_id].blank? or a[:amount].blank? }, :allow_destroy => true


   def self.debits(general_receipt_voucher)
     paid_amount = general_receipt_voucher.general_receipts.map {|s| s['amount'].to_f}.reduce(0, :+)
     Plutus::Account
     [{:account=>general_receipt_voucher.account,:amount=>paid_amount.round(2)}]
   end
   def self.credits(general_receipt_voucher)
    Plutus::Account
    credits = []
    general_receipt_voucher.general_receipts.each do |receipt|
      if receipt.tax_code.blank?
      	credits << {:account=>receipt.account_id,:amount=>receipt.amount.round(2)}
      else
      	@tax_account = Plutus::Liability.find_or_create_by(:name => receipt.tax_code.name)
      	credits << {:account=>receipt.account_id,:amount=>(receipt.amount - receipt.tax_amount.round(2)).round(2)}
      	credits << {:account=>@tax_account.id,:amount=>receipt.tax_amount.round(2)}
      end
    end
    credits
   end
  def self.make_transaction(general_receipt_voucher)
  	 debits = self.debits(general_receipt_voucher)
  	 credits = self.credits(general_receipt_voucher)
     transaction = Plutus::Transaction.build(:transaction_date => general_receipt_voucher.payment_date,:description => "Being Amount Received from #{general_receipt_voucher.payer}", :commercial_document => general_receipt_voucher,
     :debits=>debits,:credits=>credits)
    transaction
  end
  def self.update_transaction(general_receipt_voucher)
    prev_transaction = Plutus::Transaction.find(general_receipt_voucher.transaction.id)
    prev_transaction.debit_amounts.delete_all
    prev_transaction.credit_amounts.delete_all
    prev_transaction.delete
     debits = self.debits(general_receipt_voucher)
     credits = self.credits(general_receipt_voucher)
     transaction = Plutus::Transaction.build(:transaction_date => general_receipt_voucher.payment_date,:description => "Being Amount Received from #{general_receipt_voucher.payer}", :commercial_document => general_receipt_voucher,
     :debits=>debits,:credits=>credits)
     transaction
  end
  def self.search(search)
    if search
      begin
        where('payment_date = ?', search.to_date)
      rescue
        where('reference_no LIKE ?',"%#{search}%")
      end
    else
      all.order('payment_date DESC')
    end
  end   
end
