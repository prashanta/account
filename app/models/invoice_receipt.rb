class InvoiceReceipt < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :invoice_receipt_voucher
  after_save :update_invoice

  def update_invoice
  	invoice = Invoice.find(self.invoice.id)
  	received_amount = invoice.invoice_receipts.sum('amount')
    invoice.update(:amount_received=>received_amount.round(2))
  end
end
