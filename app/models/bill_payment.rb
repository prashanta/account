class BillPayment < ActiveRecord::Base
  belongs_to :bill
  belongs_to :bill_payment_voucher
  after_save :update_bill
  def update_bill
  	bill = Bill.find(self.bill.id)
  	paid_amount = bill.bill_payments.sum('amount')
    bill.update(:amount_paid=>paid_amount.round(2))
  end
end
