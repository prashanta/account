class TaxCode < ActiveRecord::Base
	has_many :invoice_items,:dependent=>:destroy
	has_many :bill_items,:dependent=>:destroy
	has_many :general_payments,:dependent=>:destroy
	has_many :general_receipts,:dependent=>:destroy
	has_many :return_invoice_items,:dependent=>:destroy
end
