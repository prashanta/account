class JournalVoucher < ActiveRecord::Base
	has_many :debit_journals,:dependent=>:destroy
	has_many :credit_journals,:dependent=>:destroy
    has_one :transaction, :as => :commercial_document,:class_name => "Plutus::Transaction"
	accepts_nested_attributes_for :debit_journals, :reject_if => lambda { |a| a[:amount].blank? or a[:account_id].blank? }, :allow_destroy => true
	accepts_nested_attributes_for :credit_journals, :reject_if => lambda { |a| a[:amount].blank? or a[:account_id].blank? }, :allow_destroy => true
    validate :amounts_cancel?
protected
    def self.debits(hash)
     debits=hash.debit_journals.group('account_id').select('SUM(amount) as amount,account_id').map{|i|{:account=>i.account_id,:amount=>i.amount.round(2)}}
     debits
    end

    def self.credits(hash)
     credits=hash.credit_journals.group('account_id').select('SUM(amount) as amount,account_id').map{|i|{:account=>i.account_id,:amount=>i.amount.round(2)}}
     credits
    end
    def self.make_transaction(hash)
    debits = self.debits(hash)
    credits = self.credits(hash)
    transaction = Plutus::Transaction.build(:transaction_date => hash.voucher_date,:description => hash.narration, :commercial_document => hash,
    :debits=>debits,:credits=>credits)
    transaction
  end
  def self.update_transaction(journal_voucher)
    prev_transaction = Plutus::Transaction.find(journal_voucher.transaction.id)
    prev_transaction.debit_amounts.delete_all
    prev_transaction.credit_amounts.delete_all
    prev_transaction.delete
    debits = self.debits(journal_voucher)
    credits = self.credits(journal_voucher)
    transaction = Plutus::Transaction.build(:transaction_date => journal_voucher.voucher_date,:description => journal_voucher.narration, :commercial_document => journal_voucher,
    :debits=>debits,:credits=>credits)
     transaction
  end
  def self.search(search)
    if search
      begin
        where('voucher_date = ?', search.to_date)
      rescue
        where('reference_no LIKE ?',"%#{search}%")
      end
    else
      all.order('voucher_date DESC')
    end
  end
  private
	def amounts_cancel?
	 errors[:base] << "The credit and debit amounts are not equal" if debit_journals.map(&:amount).compact.sum != credit_journals.map(&:amount).compact.sum
	end
end
