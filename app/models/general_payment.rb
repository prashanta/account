class GeneralPayment < ActiveRecord::Base
  belongs_to :account,:class_name => "Plutus::Account"
  belongs_to :general_payment_voucher
  belongs_to :tax_code
  before_save :validate_fields
   protected

   def validate_fields
     self.amount = 0 if self.amount.blank?
     self.tax_amount = 0 if self.tax_code.blank?
     self.tax_amount = ((self.amount*self.tax_code.rate)/(100+self.tax_code.rate)).round(2) if !self.tax_code.blank? and !self.amount.blank?
   end
end
