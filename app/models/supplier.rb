class Supplier < ActiveRecord::Base
	has_many :bills,:dependent=>:destroy

  def self.search(search)
    if search
        where('name LIKE ?',"%#{search}%")
    else
      all.order('name')
    end
  end
end
