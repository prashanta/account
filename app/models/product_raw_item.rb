class ProductRawItem < ActiveRecord::Base
  belongs_to :item
  belongs_to :production
end
