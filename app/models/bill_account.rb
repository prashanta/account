class BillAccount < ActiveRecord::Base
  belongs_to :account,:class_name => "Plutus::Account"
  belongs_to :bill
  belongs_to :tax_code
  belongs_to :item
  before_save :validate_fields

   protected

   def validate_fields
     self.qty = 1 if self.qty.blank?
     self.price = 0 if self.price.blank?
     self.amount = 0 if self.amount.blank?
     self.tax_amount = 0 if self.tax_code.blank?
     self.tax_amount = ((self.amount*self.tax_code.rate)/(100+self.tax_code.rate)).round(2) if !self.tax_code.blank? and !self.amount.blank?
   end
end
