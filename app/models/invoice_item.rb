class InvoiceItem < ActiveRecord::Base
  belongs_to :item
  belongs_to :account,:class_name => "Plutus::Account"
  belongs_to :invoice
  belongs_to :tax_code
  has_many :return_invoice_items,:dependent=>:destroy
  before_save :validate_fields
   protected

   def validate_fields
     self.qty = 1 if self.qty.blank? or self.qty == 0
     self.price = 0 if self.price.blank?
     self.amount = 0 if self.amount.blank?
     self.tax_amount = 0 if self.tax_code.blank?
     self.tax_amount = ((self.amount*self.tax_code.rate)/(100+self.tax_code.rate)).round(2) if !self.tax_code.blank? and !self.amount.blank?
   end
end
