// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require bootstrap
//= require turbolinks
//= require chosen-jquery
//= require bootstrap-datepicker/core
//= require bootstrap-datepicker/locales/bootstrap-datepicker.es
//= require bootstrap-datepicker/locales/bootstrap-datepicker.fr
//= require_tree .
jQuery(function($){
    $('.chzn-select').chosen();
});
$(document).on("focus", "[data-behaviour~='datepicker']", function(e){
    $(this).datepicker({"format": "dd-mm-yyyy", "weekStart": 1, "autoclose": true});
});
function remove_invoice_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".fields").find('input[id=invoice_invoice_items__amount]').remove();
  $(link).closest(".fields").hide();
  inv_amount =0 ;
  for(var i = 0; i < $('input[id=invoice_invoice_items__amount]').length; i++)
     {
     	if(isNaN(parseInt($('input[id=invoice_invoice_items__amount]')[i].value)))
     	  inv_amount += 0;
     	else
         inv_amount += parseFloat($('input[id=invoice_invoice_items__amount]')[i].value);
     }
      $('input[id=invoive_amount]').val((parseFloat(inv_amount)).toFixed(2));
}
function remove_bill_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".fields").find('input[id=bill_bill_accounts_amount]').remove();
  $(link).closest(".fields").hide();
  inv_amount =0 ;
  for(var i = 0; i < $('input[id=bill_bill_accounts_amount]').length; i++)
     {
      if(isNaN(parseInt($('input[id=bill_bill_accounts_amount]')[i].value)))
        inv_amount += 0;
      else
         inv_amount += parseFloat($('input[id=bill_bill_accounts_amount]')[i].value);
     }
      $('input[id=bill_amount]').val((parseFloat(inv_amount)).toFixed(2));
}
function remove_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".fields").hide();
}
function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $(link).parent().before(content.replace(regexp, new_id));
}