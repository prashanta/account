function total_bill_amount(th)
{
 qty    = $(th).parent().prev('.field').find('input[id=bill_bill_accounts__qty]').val();
 amount = $(th).closest('.field').next('.field').find('input[id=bill_bill_accounts_amount]');
 rate = th.value;
 inv_amount = 0;
  if(qty == 0)
  	 qty = 1;
  if(rate == 0)
  	rate = 0;
   a=parseFloat(rate) * parseFloat(qty)
   amount.val(a.toFixed(2));
    for(var i = 0; i < $('input[id=bill_bill_accounts_amount]').length; i++)
     {
     	if(isNaN(parseInt($('input[id=bill_bill_accounts_amount]')[i].value)))
     	  inv_amount += 0;
     	else
         inv_amount += parseFloat($('input[id=bill_bill_accounts_amount]')[i].value);
     }
      $('input[id=bill_amount]').val((parseFloat(inv_amount)).toFixed(2));
}

function find_supplier(th)
{
  $('#billing_address').val("");
  if(th.value != "")
  {
  $.get('/find_supplier/'+$('option:selected',th).val(),'',"script");
  return false;
  }
}

$('#products').dataTable()
sPaginationType: "full_numbers"
bJQueryUI: true