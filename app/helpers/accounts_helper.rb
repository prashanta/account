module AccountsHelper
	def amount_balance(debit_amount,credit_amount)
		if debit_amount.sum('amount') >= credit_amount.sum('amount')
          balance=number_to_currency((debit_amount.sum('amount')-credit_amount.sum('amount')).round(2),unit:'').to_s+" "+'Dr'
		else
		 balance=number_to_currency((credit_amount.sum('amount')-debit_amount.sum('amount')).round(2),unit:'').to_s+" "+'Cr'
		end
	end
end