module TrialBalancesHelper
	def debit_balance(account,trial_balance)
		debit_amount  = account.debit_transactions.where('transaction_date >= ?', trial_balance.from).where('transaction_date <= ?', trial_balance.to).sum('amount')
	    credit_amount = account.credit_transactions.where('transaction_date >= ?', trial_balance.from).where('transaction_date <= ?', trial_balance.to).sum('amount')
	    if debit_amount > credit_amount
	      return number_to_currency((debit_amount-credit_amount).round(2),unit:'')
	    else
		 return nil
		end
	end
	def credit_balance(account,trial_balance)
		debit_amount  = account.debit_transactions.where('transaction_date >= ?', trial_balance.from).where('transaction_date <= ?', trial_balance.to).sum('amount')
	    credit_amount = account.credit_transactions.where('transaction_date >= ?', trial_balance.from).where('transaction_date <= ?', trial_balance.to).sum('amount')
	    if credit_amount > debit_amount
	      return number_to_currency((credit_amount-debit_amount).round(2),unit:'')
	    else
		 return nil
		end
	end
end
