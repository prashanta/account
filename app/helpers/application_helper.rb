module ApplicationHelper
	def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end
  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.pluralize + "_fields", :f => builder)
    end
    link_to_function(name, ("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"))
  end

  def link_to_remove_invoice_fields(name,f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_invoice_fields(this)")
  end
  def link_to_remove_bill_fields(name,f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_bill_fields(this)")
  end
  def cpath(controller)
   "current" if controller_name.eql?(controller)
   #current_page?(path)
  end
  def invoice_cpath
    "current" if controller_name.eql?('invoices') or controller_name.eql?('invoice_receipt_vouchers') or controller_name.eql?('return_invoices')
  end
  def bills_cpath
    "current" if controller_name.eql?('bills') or controller_name.eql?('bill_payment_vouchers')
  end
  def report_cpath
    "current" if controller_name.eql?('general_ledgers') or controller_name.eql?('trial_balances') or controller_name.eql?('profit_and_losses') or controller_name.eql?('balance_sheets') or controller_name.eql?('reports')
  end
  def setting_cpath
    "current" if controller_name.eql?('items') or controller_name.eql?('tax_codes') or controller_name.eql?('settings') or current_page?(accounts_path)
  end


def sortable(column, title = nil)
  title ||= column.titleize
  css_class = column == sort_column ? "current #{sort_direction}" : nil
  direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
  link_to title, {:sort => column, :direction => direction}, {:class => css_class}
end
end
