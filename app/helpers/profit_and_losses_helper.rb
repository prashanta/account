module ProfitAndLossesHelper
	def pl_income_balance(account,pl)
	  debit_amount  = account.debit_transactions.where('transaction_date >= ?', pl.from).where('transaction_date <= ?', pl.to).sum('amount')
	  credit_amount = account.credit_transactions.where('transaction_date >= ?', pl.from).where('transaction_date <= ?', pl.to).sum('amount')
	  return number_to_currency((credit_amount-debit_amount).round(2),unit:'',negative_format: "(%u%n)")
	end
	def pl_expense_balance(account,pl)
		debit_amount  = account.debit_transactions.where('transaction_date >= ?', pl.from).where('transaction_date <= ?', pl.to).sum('amount')
	    credit_amount = account.credit_transactions.where('transaction_date >= ?', pl.from).where('transaction_date <= ?', pl.to).sum('amount')
	    return number_to_currency((debit_amount-credit_amount).round(2),unit:'',negative_format: "(%u%n)")
	end
	def revenue(pl)
	  revenue_accounts = Plutus::Revenue.all.map {|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date >= ?', pl.from).where('transaction_date <= ?', pl.to).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{revenue_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return balance
	end
	def expense(pl)
	  expense_accounts = Plutus::Expense.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date >= ?', pl.from).where('transaction_date <= ?', pl.to).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{expense_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance += amount_record.amount
		  else
		    balance -= amount_record.amount
		  end
		end
        return balance
	end

	def pl_closing_stock(upto)
	  inventory_in = InventoryIn.joins(:item).where("in_date <= ?",upto).where("items.item_type= ?",'ca').sum(:amount)
	  inventory_out = InventoryOut.joins(:item).where("out_date <= ?",upto).where("items.item_type= ?",'ca').sum(:amount)
	  closing_stock = inventory_in - inventory_out
	  return closing_stock.round(2)
    end
	def pl_opening_stock(upto)
      inventory_in = InventoryIn.joins(:item).where("in_date < ?",upto).where("items.item_type= ?",'ca').sum(:amount)
	  inventory_out = InventoryOut.joins(:item).where("out_date < ?",upto).where("items.item_type= ?",'ca').sum(:amount)
	  opening_stock =  inventory_in - inventory_out
	  return opening_stock.round(2)
    end
	def pl_cost_of_goods_sold(from,to)
	  cost_of_goods_sold = InventoryOut.joins(:item).where("out_date >= ?",from).where("out_date <= ?",to).where("items.item_type= ?",'ca').sum(:amount)
	  return cost_of_goods_sold.round(2)
     
    end
	def profit_loss_statement(pl)
	  profit_or_loss = ((revenue(pl)+pl_closing_stock(pl.to))-(expense(pl)+pl_opening_stock(pl.from)))
	  return number_to_currency(profit_or_loss.round(2),unit:'',negative_format: "(%u%n)")
	end
end

 