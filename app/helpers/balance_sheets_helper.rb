module BalanceSheetsHelper
	def asset_balance_bl(account,balancesheet)
	  debit_amount  = account.debit_transactions.where('transaction_date <= ?', balancesheet.as_at).sum('amount')
	  credit_amount = account.credit_transactions.where('transaction_date <= ?', balancesheet.as_at).sum('amount')
	  return number_to_currency((debit_amount-credit_amount).round(2),unit:'',negative_format: "(%u%n)")
	end
	def liability_balance_bl(account,balancesheet)
	  debit_amount  = account.debit_transactions.where('transaction_date <= ?', balancesheet.as_at).sum('amount')
	  credit_amount = account.credit_transactions.where('transaction_date <= ?', balancesheet.as_at).sum('amount')
	  return number_to_currency((credit_amount-debit_amount).round(2),unit:'',negative_format: "(%u%n)")
	end
	def equity_balance_bl(account,balancesheet)
	  debit_amount  = account.debit_transactions.where('transaction_date <= ?', balancesheet.as_at).sum('amount')
	  credit_amount = account.credit_transactions.where('transaction_date <= ?', balancesheet.as_at).sum('amount')
	  return number_to_currency((credit_amount-debit_amount).round(2),unit:'',negative_format: "(%u%n)")
	end
	def assets(balancesheet)
	  asset_accounts = Plutus::Asset.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date <= ?', balancesheet.as_at).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{asset_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance += amount_record.amount
		  else
		    balance -= amount_record.amount
		  end
		end
        return balance		
	end
	def liability(balancesheet)
	  liability_accounts = Plutus::Liability.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date <= ?', balancesheet.as_at).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{liability_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return balance
	end

	def net_assets(balancesheet)
	  netAssets = ((assets(balancesheet)+pl_closing_stock(balancesheet.as_at))-liability(balancesheet))
	  return number_to_currency(netAssets.round(2),unit:'',negative_format: "(%u%n)")
	end
	def balancesheet_revenue(balancesheet)
	  revenue_accounts = Plutus::Revenue.all.map {|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date <= ?', balancesheet.as_at).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{revenue_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return balance
	end
	def balancesheet_expense(balancesheet)
	  expense_accounts = Plutus::Expense.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date <= ?', balancesheet.as_at).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{expense_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance += amount_record.amount
		  else
		    balance -= amount_record.amount
		  end
		end
        return balance
	end
	
	def net_profit_as_at(balancesheet)
	  #profit_or_loss = balancesheet_revenue(balancesheet)-balancesheet_expense(balancesheet)
	   #return profit_or_loss.round(2)
	   profit_or_loss = (balancesheet_revenue(balancesheet)+pl_closing_stock(balancesheet.as_at))-(balancesheet_expense(balancesheet))
	  return profit_or_loss.round(2)
	end

	def net_equity(balancesheet)
	  equity_accounts = Plutus::Equity.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date <= ?', balancesheet.as_at).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{equity_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
		net_equity = balance+net_profit_as_at(balancesheet) 
        return number_to_currency(net_equity.round(2),unit:'',negative_format: "(%u%n)")
	end
end
