module SummariesHelper
	@first_transaction_date = if Plutus::Transaction.first.nil? then Date.today else Plutus::Transaction.first.transaction_date end
	@date_from = if Summary.last.nil? then @first_transaction_date else Summary.last.date_from end
	def asset_balance
	  asset_accounts = Plutus::Asset.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{asset_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance += amount_record.amount
		  else
		    balance -= amount_record.amount
		  end
		end
        return number_to_currency(balance+pl_closing_stock(Date.today),unit:'',negative_format: "(%u%n)")		
	end
	def summary_asset_balance(account)
		period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
		transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
        period_amounts = Plutus::Amount.where(:account_id => account.id).where("transaction_id IN (#{transaction_id_string})")
	    balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance += amount_record.amount
		  else
		    balance -= amount_record.amount
		  end
		end
        return number_to_currency(balance,unit:'',negative_format: "(%u%n)")
	end
	def liability_balance
	  liability_accounts = Plutus::Liability.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{liability_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return number_to_currency(balance,unit:'',negative_format: "(%u%n)")		
	end
	def summary_liability_balance(account)
		period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
		transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
        period_amounts = Plutus::Amount.where(:account_id => account.id).where("transaction_id IN (#{transaction_id_string})")
	    balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return number_to_currency(balance,unit:'',negative_format: "(%u%n)")
	end
	def equity_balance
	  equity_accounts = Plutus::Equity.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{equity_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return number_to_currency(balance,unit:'',negative_format: "(%u%n)")		
	end
	def summary_equity_balance(account)
		period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
		transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
        period_amounts = Plutus::Amount.where(:account_id => account.id).where("transaction_id IN (#{transaction_id_string})")
	    balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return number_to_currency(balance,unit:'',negative_format: "(%u%n)")
	end
	def expense_balance
	  expense_accounts = Plutus::Expense.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{expense_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance += amount_record.amount
		  else
		    balance -= amount_record.amount
		  end
		end
        return balance	
	end
	def summary_expense_balance(account)
		period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
		transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
        period_amounts = Plutus::Amount.where(:account_id => account.id).where("transaction_id IN (#{transaction_id_string})")
	    balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance += amount_record.amount
		  else
		    balance -= amount_record.amount
		  end
		end
        return number_to_currency(balance,unit:'',negative_format: "(%u%n)")
	end
	def income_balance
	  revenue_accounts = Plutus::Revenue.all.map{|t| t.id}.join(', ')
	  period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
	  transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
      period_amounts = Plutus::Amount.where("account_id IN (#{revenue_accounts})").where("transaction_id IN (#{transaction_id_string})")
	  balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return balance		
	end
	def summary_income_balance(account)
		period_transactions = Plutus::Transaction.where('transaction_date >= ?',@date_from).select(:id)
		transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
        period_amounts = Plutus::Amount.where(:account_id => account.id).where("transaction_id IN (#{transaction_id_string})")
	    balance = BigDecimal.new('0')
		period_amounts.each do |amount_record|
		  if amount_record.type == "Plutus::DebitAmount"
		    balance -= amount_record.amount
		  else
		    balance += amount_record.amount
		  end
		end
        return number_to_currency(balance,unit:'',negative_format: "(%u%n)")
	end

	def summary_profit_loss_statement
		profit_or_loss=((income_balance.to_f+pl_closing_stock(Date.today))-(expense_balance.to_f+pl_opening_stock(@date_from)))
	  return number_to_currency(profit_or_loss.round(2),unit:'',negative_format: "(%u%n)")
	end
end
