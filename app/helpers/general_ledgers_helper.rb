module GeneralLedgersHelper
	def ledger_debit_balance(account,ledger)
	debit_amount  = account.debit_transactions.where('transaction_date >= ?', ledger.from).where('transaction_date <= ?', ledger.to).sum('amount')
    return number_to_currency(debit_amount.round(2),unit:'')
   end
	def ledger_credit_balance(account,ledger)
	    credit_amount = account.credit_transactions.where('transaction_date >= ?', ledger.from).where('transaction_date <= ?', ledger.to).sum('amount')
	      return number_to_currency(credit_amount.round(2),unit:'')
	end

	def ledger_balance(account,ledger)
		debit_amount  = account.debit_transactions.where('transaction_date >= ?', ledger.from).where('transaction_date <= ?', ledger.to).sum('amount')
	    credit_amount = account.credit_transactions.where('transaction_date >= ?', ledger.from).where('transaction_date <= ?', ledger.to).sum('amount')
        if debit_amount > credit_amount
        	balance = number_to_currency((debit_amount-credit_amount).round(2),unit:'').to_s+" "+"Dr"
        else
        	balance = number_to_currency((credit_amount-debit_amount).round(2),unit: '').to_s+" "+"Cr"
        end
        return balance
	end

	def total_ledger_debit(ledger)
		period_transactions = Plutus::Transaction.where('transaction_date >= ?', ledger.from).where('transaction_date <= ?', ledger.to).select(:id)
        transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
		debitamount=Plutus::DebitAmount.where("transaction_id IN (#{transaction_id_string})").sum('amount')
	    return number_to_currency(debitamount.round(2),unit:'')
	end
	def total_ledger_credit(ledger)
		period_transactions = Plutus::Transaction.where('transaction_date >= ?', ledger.from).where('transaction_date <= ?', ledger.to).select(:id)
        transaction_id_string = period_transactions.map {|t| t.id}.join(', ')
		creditamount=Plutus::CreditAmount.where("transaction_id IN (#{transaction_id_string})").sum('amount')
	    return number_to_currency(creditamount.round(2),unit:'')
	end
end
