module InvoicesHelper
	def balance_due(invoice)
      balance_due = invoice.amount.to_f
      if invoice.return_invoices.nil?
      	balance_due = balance_due
      else
      	balance_due = balance_due - invoice.return_invoices.sum('amount')
      end
      balance_due -= invoice.amount_received.to_f

      return balance_due
	end
	def invoice_balance_due(invoice)
      balance_due = invoice.amount.to_f
      invoice.return_invoices.each do |return_invoice|
        if return_invoice.transactions.count > 0
        	balance_due -= return_invoice.amount.to_f
        else
        	balance_due = balance_due
        end
      end 
        balance_due -= invoice.amount_received.to_f
        return balance_due 
	end	

  def return_invoices(return_invoices)
   return_amount = BigDecimal.new('0')
   return_invoices.each do |return_invoice|
   return_amount += Plutus::CreditAmount.where('transaction_id IN(?)',return_invoice.transactions).sum('amount')
   end
   return return_amount
  end
end
