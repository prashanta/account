Account::Application.routes.draw do

  resources :productions,:except=>[:destroy] do
    member do
      get :approved
    end
  end

  resources :summaries,:only=>[:index,:edit,:update]

  resources :bill_return_vouchers,:except=>[:destroy]

  resources :invoice_return_vouchers,:except=>[:destroy]

  resources :balance_sheets,:except=>[:destroy]

  resources :profit_and_losses,:except=>[:destroy]

  resources :general_ledgers,:except=>[:destroy]

  resources :trial_balances,:except=>[:destroy]
  resources :general_receipt_vouchers,:except=>[:destroy,:show]

  resources :invoice_receipt_vouchers,:except=>[:destroy,:index,:show]

  resources :general_payment_vouchers,:except=>[:destroy,:show]

  resources :bill_payment_vouchers,:except=>[:destroy,:index,:show]

  resources :journal_vouchers,:except=>[:destroy,:show] 

  resources :bills,:except=>[:destroy] do
  member do
    get :transaction
  end
  end

  resources :invoice_items,:except=>[:destroy]

  resources :invoices,:except=>[:destroy] do
    member do
      get :transaction
    end
  end
  resources :return_invoices,:except=>[:index,:destroy] do
  member do
    get :transaction
  end
  end
  resources :customers,:except=>[:destroy]

  resources :suppliers,:except=>[:destroy]

  resources :tax_codes,:except=>[:show,:destroy]

  resources :items,:except=>[:show,:destroy]

  resources :accounts,:except=>[:destroy]

  get "find_customer/:id" => "customers#find_customer"
  get "find_supplier/:id" => "suppliers#find_supplier"
  get 'payment' => "payment_vouchers#index"
  get 'report'  => "reports#index"
  get "settings" =>"settings#index"
  get "inventory" =>"inventory#index"
  root 'summaries#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
