# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131125115903) do

  create_table "balance_sheets", force: true do |t|
    t.date     "as_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bill_items", force: true do |t|
    t.integer  "account_id"
    t.integer  "bill_id"
    t.integer  "tax_code_id"
    t.float    "tax_amount"
    t.integer  "item_id"
    t.float    "qty"
    t.float    "price"
    t.decimal  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bill_items", ["account_id"], name: "index_bill_items_on_account_id"
  add_index "bill_items", ["bill_id"], name: "index_bill_items_on_bill_id"
  add_index "bill_items", ["item_id"], name: "index_bill_items_on_item_id"
  add_index "bill_items", ["tax_code_id"], name: "index_bill_items_on_tax_code_id"

  create_table "bill_payment_vouchers", force: true do |t|
    t.string   "reference_no"
    t.date     "payment_date"
    t.string   "payee"
    t.integer  "account_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bill_payment_vouchers", ["account_id"], name: "index_bill_payment_vouchers_on_account_id"

  create_table "bill_payments", force: true do |t|
    t.integer  "bill_id"
    t.decimal  "amount"
    t.integer  "bill_payment_voucher_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bill_payments", ["bill_id"], name: "index_bill_payments_on_bill_id"
  add_index "bill_payments", ["bill_payment_voucher_id"], name: "index_bill_payments_on_bill_payment_voucher_id"

  create_table "bills", force: true do |t|
    t.date     "issue_date"
    t.date     "due_date"
    t.integer  "supplier_id"
    t.string   "reference_no"
    t.text     "internal_information"
    t.decimal  "amount"
    t.decimal  "amount_paid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bills", ["supplier_id"], name: "index_bills_on_supplier_id"

  create_table "credit_journals", force: true do |t|
    t.integer  "journal_voucher_id"
    t.integer  "account_id"
    t.string   "description"
    t.decimal  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "credit_journals", ["account_id"], name: "index_credit_journals_on_account_id"
  add_index "credit_journals", ["journal_voucher_id"], name: "index_credit_journals_on_journal_voucher_id"

  create_table "customers", force: true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "email"
    t.string   "telephone"
    t.string   "mobile"
    t.text     "additional_information"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "debit_journals", force: true do |t|
    t.integer  "journal_voucher_id"
    t.integer  "account_id"
    t.string   "description"
    t.decimal  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "debit_journals", ["account_id"], name: "index_debit_journals_on_account_id"
  add_index "debit_journals", ["journal_voucher_id"], name: "index_debit_journals_on_journal_voucher_id"

  create_table "general_ledgers", force: true do |t|
    t.date     "from"
    t.date     "to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "general_payment_vouchers", force: true do |t|
    t.string   "reference_no"
    t.date     "payment_date"
    t.string   "payee"
    t.integer  "account_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "general_payment_vouchers", ["account_id"], name: "index_general_payment_vouchers_on_account_id"

  create_table "general_payments", force: true do |t|
    t.integer  "account_id"
    t.decimal  "amount"
    t.integer  "general_payment_voucher_id"
    t.integer  "tax_code_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "tax_amount"
  end

  add_index "general_payments", ["account_id"], name: "index_general_payments_on_account_id"
  add_index "general_payments", ["general_payment_voucher_id"], name: "index_general_payments_on_general_payment_voucher_id"
  add_index "general_payments", ["tax_code_id"], name: "index_general_payments_on_tax_code_id"

  create_table "general_receipt_vouchers", force: true do |t|
    t.string   "reference_no"
    t.date     "payment_date"
    t.string   "payer"
    t.integer  "account_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "general_receipt_vouchers", ["account_id"], name: "index_general_receipt_vouchers_on_account_id"

  create_table "general_receipts", force: true do |t|
    t.integer  "account_id"
    t.decimal  "amount"
    t.integer  "general_receipt_voucher_id"
    t.integer  "tax_code_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "tax_amount"
  end

  add_index "general_receipts", ["account_id"], name: "index_general_receipts_on_account_id"
  add_index "general_receipts", ["general_receipt_voucher_id"], name: "index_general_receipts_on_general_receipt_voucher_id"
  add_index "general_receipts", ["tax_code_id"], name: "index_general_receipts_on_tax_code_id"

  create_table "inventory_ins", force: true do |t|
    t.integer  "item_id"
    t.float    "quantity"
    t.float    "rate"
    t.decimal  "amount"
    t.float    "in_stock"
    t.integer  "commercial_document_id"
    t.string   "commercial_document_type"
    t.date     "in_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "inventory_ins", ["item_id"], name: "index_inventory_ins_on_item_id"

  create_table "inventory_outs", force: true do |t|
    t.integer  "item_id"
    t.float    "quantity"
    t.float    "rate"
    t.decimal  "amount"
    t.integer  "commercial_document_id"
    t.string   "commercial_document_type"
    t.date     "out_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "inventory_outs", ["item_id"], name: "index_inventory_outs_on_item_id"

  create_table "invoice_items", force: true do |t|
    t.integer  "item_id"
    t.integer  "account_id"
    t.float    "qty"
    t.float    "price"
    t.decimal  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "invoice_id"
    t.integer  "tax_code_id"
    t.float    "tax_amount"
  end

  add_index "invoice_items", ["account_id"], name: "index_invoice_items_on_account_id"
  add_index "invoice_items", ["invoice_id"], name: "index_invoice_items_on_invoice_id"
  add_index "invoice_items", ["item_id"], name: "index_invoice_items_on_item_id"
  add_index "invoice_items", ["tax_code_id"], name: "index_invoice_items_on_tax_code_id"

  create_table "invoice_receipt_vouchers", force: true do |t|
    t.string   "reference_no"
    t.date     "payment_date"
    t.string   "payer"
    t.integer  "account_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoice_receipt_vouchers", ["account_id"], name: "index_invoice_receipt_vouchers_on_account_id"

  create_table "invoice_receipts", force: true do |t|
    t.integer  "invoice_id"
    t.decimal  "amount"
    t.integer  "invoice_receipt_voucher_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoice_receipts", ["invoice_id"], name: "index_invoice_receipts_on_invoice_id"
  add_index "invoice_receipts", ["invoice_receipt_voucher_id"], name: "index_invoice_receipts_on_invoice_receipt_voucher_id"

  create_table "invoices", force: true do |t|
    t.string   "invoice_no"
    t.date     "issue_date"
    t.date     "due_date"
    t.integer  "customer_id"
    t.text     "internal_information"
    t.decimal  "amount"
    t.decimal  "amount_received"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoices", ["customer_id"], name: "index_invoices_on_customer_id"

  create_table "items", force: true do |t|
    t.string   "name"
    t.string   "unit"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "item_type"
    t.string   "category"
  end

  create_table "journal_vouchers", force: true do |t|
    t.date     "voucher_date"
    t.string   "reference_no"
    t.string   "narration"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plutus_accounts", force: true do |t|
    t.string   "name"
    t.string   "type"
    t.boolean  "contra"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_editable"
  end

  add_index "plutus_accounts", ["name", "type"], name: "index_plutus_accounts_on_name_and_type"

  create_table "plutus_amounts", force: true do |t|
    t.string  "type"
    t.integer "account_id"
    t.integer "transaction_id"
    t.decimal "amount",         precision: 20, scale: 10
  end

  add_index "plutus_amounts", ["account_id", "transaction_id"], name: "index_plutus_amounts_on_account_id_and_transaction_id"
  add_index "plutus_amounts", ["transaction_id", "account_id"], name: "index_plutus_amounts_on_transaction_id_and_account_id"
  add_index "plutus_amounts", ["type"], name: "index_plutus_amounts_on_type"

  create_table "plutus_transactions", force: true do |t|
    t.string   "description"
    t.integer  "commercial_document_id"
    t.string   "commercial_document_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "transaction_date"
  end

  add_index "plutus_transactions", ["commercial_document_id", "commercial_document_type"], name: "index_transactions_on_commercial_doc"

  create_table "product_items", force: true do |t|
    t.integer  "item_id"
    t.integer  "production_id"
    t.float    "qty"
    t.float    "mrp"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  add_index "product_items", ["item_id"], name: "index_product_items_on_item_id"
  add_index "product_items", ["production_id"], name: "index_product_items_on_production_id"

  create_table "product_raw_items", force: true do |t|
    t.integer  "item_id"
    t.integer  "production_id"
    t.float    "qty"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  add_index "product_raw_items", ["item_id"], name: "index_product_raw_items_on_item_id"
  add_index "product_raw_items", ["production_id"], name: "index_product_raw_items_on_production_id"

  create_table "productions", force: true do |t|
    t.string   "batch_no"
    t.date     "production_date"
    t.boolean  "approved"
    t.text     "internal_information"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profit_and_losses", force: true do |t|
    t.date     "from"
    t.date     "to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "return_invoice_items", force: true do |t|
    t.integer  "return_invoice_id"
    t.integer  "invoice_item_id"
    t.integer  "tax_code_id"
    t.float    "quantity"
    t.float    "rate"
    t.decimal  "amount"
    t.decimal  "tax_amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "return_invoice_items", ["invoice_item_id"], name: "index_return_invoice_items_on_invoice_item_id"
  add_index "return_invoice_items", ["return_invoice_id"], name: "index_return_invoice_items_on_return_invoice_id"
  add_index "return_invoice_items", ["tax_code_id"], name: "index_return_invoice_items_on_tax_code_id"

  create_table "return_invoices", force: true do |t|
    t.integer  "invoice_id"
    t.decimal  "amount"
    t.date     "return_date"
    t.string   "reference_no"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "return_invoices", ["invoice_id"], name: "index_return_invoices_on_invoice_id"

  create_table "summaries", force: true do |t|
    t.date     "date_from"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "suppliers", force: true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "email"
    t.string   "telephone"
    t.string   "mobile"
    t.text     "additional_information"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tax_codes", force: true do |t|
    t.string   "name"
    t.float    "rate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trial_balances", force: true do |t|
    t.date     "from"
    t.date     "to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
