# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Plutus::Account
Plutus::Asset.create(:name=>'Cash at Bank',:contra=>true,:is_editable=>false)
Plutus::Asset.create(:name=>'Cash on Hand',:contra=>true,:is_editable=>false)
Plutus::Asset.create(:name=>'Account receivable',:is_editable=>false)
Plutus::Equity.create(:name => "Drawing", :contra => true,:is_editable=>false)
Plutus::Revenue.create(:name => "Sales",:is_editable=>false)
Plutus::Revenue.create(:name => "Interest received",:is_editable=>false)
Plutus::Liability.create(:name => "Account Payable",:is_editable=>false)
Plutus::Liability.create(:name => "Sales Tax Payable",:is_editable=>false)
Plutus::Liability.create(:name => "Service Tax",:is_editable=>false)
Plutus::Expense.create(:name => "Accounting Fees",:is_editable=>false)
Plutus::Expense.create(:name => "Advertising and promotion",:is_editable=>false)
Plutus::Expense.create(:name => "Bank charges",:is_editable=>false)
Plutus::Expense.create(:name => "Electricity",:is_editable=>false)
Plutus::Expense.create(:name => "Motor vehicle expenses",:is_editable=>false)
Plutus::Expense.create(:name => "Printing and stationery",:is_editable=>false)
Plutus::Expense.create(:name => "Rent",:is_editable=>false)
Plutus::Expense.create(:name => "Repairs and maintenance",:is_editable=>false)
Plutus::Expense.create(:name => "Telephone",:is_editable=>false)
Plutus::Expense.create(:name => "Computer equipment",:is_editable=>false)
Plutus::Expense.create(:name => "Purchase",:is_editable=>false)
Summary.create(:date_from=>'2012-04-01')
Supplier.create(name: 'RUNGAMATTEE TEA & INDUSTRIES LTD.',address: '90/31, Diamond Harbour Road, Kolkata-700 038',email: ' rungamatteetea@gmail.com',telephone: ' 033-24583289/3475',mobile: '0000000000')
TaxCode.create(name: 'VAT',rate: 5.00)
TaxCode.create(name: 'VAT',rate: 4.00)