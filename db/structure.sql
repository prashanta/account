CREATE TABLE "balance_sheets" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "as_at" date, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "bill_items" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "account_id" integer, "bill_id" integer, "tax_code_id" integer, "tax_amount" float, "item_id" integer, "qty" float, "price" float, "amount" decimal, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "bill_payment_vouchers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "reference_no" varchar(255), "payment_date" date, "payee" varchar(255), "account_id" integer, "description" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "bill_payments" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "bill_id" integer, "amount" decimal, "bill_payment_voucher_id" integer, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "bills" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "issue_date" date, "due_date" date, "supplier_id" integer, "reference_no" varchar(255), "internal_information" text, "amount" decimal, "amount_paid" decimal, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "credit_journals" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "journal_voucher_id" integer, "account_id" integer, "description" varchar(255), "amount" decimal, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "customers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255), "address" text, "email" varchar(255), "telephone" varchar(255), "mobile" varchar(255), "additional_information" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "debit_journals" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "journal_voucher_id" integer, "account_id" integer, "description" varchar(255), "amount" decimal, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "general_ledgers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "from" date, "to" date, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "general_payment_vouchers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "reference_no" varchar(255), "payment_date" date, "payee" varchar(255), "account_id" integer, "description" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "general_payments" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "account_id" integer, "amount" decimal, "general_payment_voucher_id" integer, "tax_code_id" integer, "created_at" datetime, "updated_at" datetime, "tax_amount" float);
CREATE TABLE "general_receipt_vouchers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "reference_no" varchar(255), "payment_date" date, "payer" varchar(255), "account_id" integer, "description" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "general_receipts" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "account_id" integer, "amount" decimal, "general_receipt_voucher_id" integer, "tax_code_id" integer, "created_at" datetime, "updated_at" datetime, "tax_amount" float);
CREATE TABLE "inventory_ins" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "item_id" integer, "quantity" float, "rate" float, "amount" decimal, "in_stock" float, "commercial_document_id" integer, "commercial_document_type" varchar(255), "in_date" date, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "inventory_outs" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "item_id" integer, "quantity" float, "rate" float, "amount" decimal, "commercial_document_id" integer, "commercial_document_type" varchar(255), "out_date" date, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "invoice_items" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "item_id" integer, "account_id" integer, "qty" float, "price" float, "amount" decimal, "created_at" datetime, "updated_at" datetime, "invoice_id" integer, "tax_code_id" integer, "tax_amount" float);
CREATE TABLE "invoice_receipt_vouchers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "reference_no" varchar(255), "payment_date" date, "payer" varchar(255), "account_id" integer, "description" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "invoice_receipts" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "invoice_id" integer, "amount" decimal, "invoice_receipt_voucher_id" integer, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "invoices" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "invoice_no" varchar(255), "issue_date" date, "due_date" date, "customer_id" integer, "internal_information" text, "amount" decimal, "amount_received" decimal, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "items" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255), "unit" varchar(255), "description" text, "created_at" datetime, "updated_at" datetime, "item_type" varchar(255), "category" varchar(255));
CREATE TABLE "journal_vouchers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "voucher_date" date, "reference_no" varchar(255), "narration" varchar(255), "notes" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "plutus_accounts" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255), "type" varchar(255), "contra" boolean, "created_at" datetime, "updated_at" datetime, "is_editable" boolean);
CREATE TABLE "plutus_amounts" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "type" varchar(255), "account_id" integer, "transaction_id" integer, "amount" decimal(20,10));
CREATE TABLE "plutus_transactions" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "description" varchar(255), "commercial_document_id" integer, "commercial_document_type" varchar(255), "created_at" datetime, "updated_at" datetime, "transaction_date" date);
CREATE TABLE "product_items" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "item_id" integer, "production_id" integer, "qty" float, "mrp" float, "created_at" datetime, "updated_at" datetime, "description" varchar(255));
CREATE TABLE "product_raw_items" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "item_id" integer, "production_id" integer, "qty" float, "created_at" datetime, "updated_at" datetime, "description" varchar(255));
CREATE TABLE "productions" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "batch_no" varchar(255), "production_date" date, "approved" boolean, "internal_information" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "profit_and_losses" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "from" date, "to" date, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "schema_migrations" ("version" varchar(255) NOT NULL);
CREATE TABLE "summaries" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "date_from" date, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "suppliers" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255), "address" text, "email" varchar(255), "telephone" varchar(255), "mobile" varchar(255), "additional_information" text, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "tax_codes" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar(255), "rate" float, "created_at" datetime, "updated_at" datetime);
CREATE TABLE "trial_balances" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "from" date, "to" date, "created_at" datetime, "updated_at" datetime);
CREATE INDEX "index_bill_items_on_account_id" ON "bill_items" ("account_id");
CREATE INDEX "index_bill_items_on_bill_id" ON "bill_items" ("bill_id");
CREATE INDEX "index_bill_items_on_item_id" ON "bill_items" ("item_id");
CREATE INDEX "index_bill_items_on_tax_code_id" ON "bill_items" ("tax_code_id");
CREATE INDEX "index_bill_payment_vouchers_on_account_id" ON "bill_payment_vouchers" ("account_id");
CREATE INDEX "index_bill_payments_on_bill_id" ON "bill_payments" ("bill_id");
CREATE INDEX "index_bill_payments_on_bill_payment_voucher_id" ON "bill_payments" ("bill_payment_voucher_id");
CREATE INDEX "index_bills_on_supplier_id" ON "bills" ("supplier_id");
CREATE INDEX "index_credit_journals_on_account_id" ON "credit_journals" ("account_id");
CREATE INDEX "index_credit_journals_on_journal_voucher_id" ON "credit_journals" ("journal_voucher_id");
CREATE INDEX "index_debit_journals_on_account_id" ON "debit_journals" ("account_id");
CREATE INDEX "index_debit_journals_on_journal_voucher_id" ON "debit_journals" ("journal_voucher_id");
CREATE INDEX "index_general_payment_vouchers_on_account_id" ON "general_payment_vouchers" ("account_id");
CREATE INDEX "index_general_payments_on_account_id" ON "general_payments" ("account_id");
CREATE INDEX "index_general_payments_on_general_payment_voucher_id" ON "general_payments" ("general_payment_voucher_id");
CREATE INDEX "index_general_payments_on_tax_code_id" ON "general_payments" ("tax_code_id");
CREATE INDEX "index_general_receipt_vouchers_on_account_id" ON "general_receipt_vouchers" ("account_id");
CREATE INDEX "index_general_receipts_on_account_id" ON "general_receipts" ("account_id");
CREATE INDEX "index_general_receipts_on_general_receipt_voucher_id" ON "general_receipts" ("general_receipt_voucher_id");
CREATE INDEX "index_general_receipts_on_tax_code_id" ON "general_receipts" ("tax_code_id");
CREATE INDEX "index_inventory_ins_on_item_id" ON "inventory_ins" ("item_id");
CREATE INDEX "index_inventory_outs_on_item_id" ON "inventory_outs" ("item_id");
CREATE INDEX "index_invoice_items_on_account_id" ON "invoice_items" ("account_id");
CREATE INDEX "index_invoice_items_on_invoice_id" ON "invoice_items" ("invoice_id");
CREATE INDEX "index_invoice_items_on_item_id" ON "invoice_items" ("item_id");
CREATE INDEX "index_invoice_items_on_tax_code_id" ON "invoice_items" ("tax_code_id");
CREATE INDEX "index_invoice_receipt_vouchers_on_account_id" ON "invoice_receipt_vouchers" ("account_id");
CREATE INDEX "index_invoice_receipts_on_invoice_id" ON "invoice_receipts" ("invoice_id");
CREATE INDEX "index_invoice_receipts_on_invoice_receipt_voucher_id" ON "invoice_receipts" ("invoice_receipt_voucher_id");
CREATE INDEX "index_invoices_on_customer_id" ON "invoices" ("customer_id");
CREATE INDEX "index_plutus_accounts_on_name_and_type" ON "plutus_accounts" ("name", "type");
CREATE INDEX "index_plutus_amounts_on_account_id_and_transaction_id" ON "plutus_amounts" ("account_id", "transaction_id");
CREATE INDEX "index_plutus_amounts_on_transaction_id_and_account_id" ON "plutus_amounts" ("transaction_id", "account_id");
CREATE INDEX "index_plutus_amounts_on_type" ON "plutus_amounts" ("type");
CREATE INDEX "index_product_items_on_item_id" ON "product_items" ("item_id");
CREATE INDEX "index_product_items_on_production_id" ON "product_items" ("production_id");
CREATE INDEX "index_product_raw_items_on_item_id" ON "product_raw_items" ("item_id");
CREATE INDEX "index_product_raw_items_on_production_id" ON "product_raw_items" ("production_id");
CREATE INDEX "index_transactions_on_commercial_doc" ON "plutus_transactions" ("commercial_document_id", "commercial_document_type");
CREATE UNIQUE INDEX "unique_schema_migrations" ON "schema_migrations" ("version");
INSERT INTO schema_migrations (version) VALUES ('20131021055614');

INSERT INTO schema_migrations (version) VALUES ('20131021071649');

INSERT INTO schema_migrations (version) VALUES ('20131021123244');

INSERT INTO schema_migrations (version) VALUES ('20131021125633');

INSERT INTO schema_migrations (version) VALUES ('20131022053208');

INSERT INTO schema_migrations (version) VALUES ('20131022053256');

INSERT INTO schema_migrations (version) VALUES ('20131022062523');

INSERT INTO schema_migrations (version) VALUES ('20131022063032');

INSERT INTO schema_migrations (version) VALUES ('20131024111146');

INSERT INTO schema_migrations (version) VALUES ('20131025113551');

INSERT INTO schema_migrations (version) VALUES ('20131028082618');

INSERT INTO schema_migrations (version) VALUES ('20131028094828');

INSERT INTO schema_migrations (version) VALUES ('20131028105534');

INSERT INTO schema_migrations (version) VALUES ('20131028105837');

INSERT INTO schema_migrations (version) VALUES ('20131028105854');

INSERT INTO schema_migrations (version) VALUES ('20131030131758');

INSERT INTO schema_migrations (version) VALUES ('20131030132044');

INSERT INTO schema_migrations (version) VALUES ('20131030132154');

INSERT INTO schema_migrations (version) VALUES ('20131030132241');

INSERT INTO schema_migrations (version) VALUES ('20131031065720');

INSERT INTO schema_migrations (version) VALUES ('20131031065816');

INSERT INTO schema_migrations (version) VALUES ('20131031065906');

INSERT INTO schema_migrations (version) VALUES ('20131031070013');

INSERT INTO schema_migrations (version) VALUES ('20131031083244');

INSERT INTO schema_migrations (version) VALUES ('20131101051611');

INSERT INTO schema_migrations (version) VALUES ('20131101073431');

INSERT INTO schema_migrations (version) VALUES ('20131101105034');

INSERT INTO schema_migrations (version) VALUES ('20131103071650');

INSERT INTO schema_migrations (version) VALUES ('20131105055219');

INSERT INTO schema_migrations (version) VALUES ('20131106052408');

INSERT INTO schema_migrations (version) VALUES ('20131106060424');

INSERT INTO schema_migrations (version) VALUES ('20131106074524');

INSERT INTO schema_migrations (version) VALUES ('20131106074739');

INSERT INTO schema_migrations (version) VALUES ('20131106091630');

INSERT INTO schema_migrations (version) VALUES ('20131106094103');

INSERT INTO schema_migrations (version) VALUES ('20131106105938');

INSERT INTO schema_migrations (version) VALUES ('20131107044539');

INSERT INTO schema_migrations (version) VALUES ('20131107044742');

INSERT INTO schema_migrations (version) VALUES ('20131116051315');

INSERT INTO schema_migrations (version) VALUES ('20131119045124');

INSERT INTO schema_migrations (version) VALUES ('20131119045254');

INSERT INTO schema_migrations (version) VALUES ('20131120050739');

INSERT INTO schema_migrations (version) VALUES ('20131120051458');

INSERT INTO schema_migrations (version) VALUES ('20131120051650');

INSERT INTO schema_migrations (version) VALUES ('20131120081744');

INSERT INTO schema_migrations (version) VALUES ('20131120085933');

INSERT INTO schema_migrations (version) VALUES ('20131120090711');

INSERT INTO schema_migrations (version) VALUES ('20131120092140');
