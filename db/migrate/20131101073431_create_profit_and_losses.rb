class CreateProfitAndLosses < ActiveRecord::Migration
  def change
    create_table :profit_and_losses do |t|
      t.date :from
      t.date :to

      t.timestamps
    end
  end
end
