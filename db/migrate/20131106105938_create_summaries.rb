class CreateSummaries < ActiveRecord::Migration
  def change
    create_table :summaries do |t|
      t.date :date_from

      t.timestamps
    end
  end
end
