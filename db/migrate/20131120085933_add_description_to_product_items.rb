class AddDescriptionToProductItems < ActiveRecord::Migration
  def change
    add_column :product_items, :description, :string
  end
end
