class CreateCreditJournals < ActiveRecord::Migration
  def change
    create_table :credit_journals do |t|
      t.references :journal_voucher, index: true
      t.references :account, index: true
      t.string :description
      t.decimal :amount

      t.timestamps
    end
  end
end
