class CreateInvoiceItems < ActiveRecord::Migration
  def change
    create_table :invoice_items do |t|
      t.references :item, index: true
      t.text :description
      t.references :account, index: true
      t.float :qty
      t.float :price
      t.decimal :amount

      t.timestamps
    end
  end
end
