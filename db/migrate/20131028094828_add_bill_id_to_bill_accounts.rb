class AddBillIdToBillAccounts < ActiveRecord::Migration
  def change
    add_reference :bill_accounts, :bill, index: true
  end
end
