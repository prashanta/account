class CreateBillPayments < ActiveRecord::Migration
  def change
    create_table :bill_payments do |t|
      t.references :bill, index: true
      t.decimal :amount
      t.references :bill_payment_voucher, index: true

      t.timestamps
    end
  end
end
