class ChangeFloatTaxAmountInInvoice < ActiveRecord::Migration
  def change
  	change_column :invoices, :tax_amount, :float
  end
end
