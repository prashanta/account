class AddDescriptionToProductRawItems < ActiveRecord::Migration
  def change
    add_column :product_raw_items, :description, :string
  end
end
