class AddTransactionDateToPlutusTransactions < ActiveRecord::Migration
  def change
  	add_column :plutus_transactions, :transaction_date, :date
  end
end
