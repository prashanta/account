class CreateBillItems < ActiveRecord::Migration
  def change
    create_table :bill_items do |t|
      t.references :account, index: true
      t.references :bill, index: true
      t.references :tax_code, index: true
      t.float :tax_amount
      t.references :item, index: true
      t.float :qty
      t.float :price
      t.decimal :amount

      t.timestamps
    end
  end
end
