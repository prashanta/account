class CreateTrialBalances < ActiveRecord::Migration
  def change
    create_table :trial_balances do |t|
      t.date :from
      t.date :to

      t.timestamps
    end
  end
end
