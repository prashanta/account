class CreateInventoryIns < ActiveRecord::Migration
  def change
    create_table :inventory_ins do |t|
      t.references :item, index: true
      t.float :quantity
      t.float :rate
      t.decimal :amount
      t.float :in_stock
      t.integer :commercial_document_id
      t.string :commercial_document_type
      t.date :in_date

      t.timestamps
    end
  end
end
