class AddInvoiceIdToInvoiceItems < ActiveRecord::Migration
  def change
    add_reference :invoice_items, :invoice, index: true
  end
end
