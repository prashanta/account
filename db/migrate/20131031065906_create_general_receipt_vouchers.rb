class CreateGeneralReceiptVouchers < ActiveRecord::Migration
  def change
    create_table :general_receipt_vouchers do |t|
      t.string :reference_no
      t.date :payment_date
      t.string :payer
      t.references :account, index: true
      t.text :description

      t.timestamps
    end
  end
end
