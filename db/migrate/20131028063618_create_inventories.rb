class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.references :item, index: true
      t.float :quantity
      t.float :rate

      t.timestamps
    end
  end
end
