class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :invoice_no
      t.date :issue_date
      t.date :due_date
      t.references :customer, index: true
      t.text :internal_information
      t.references :tax_code, index: true
      t.integer :tax_amount
      t.decimal :amount
      t.decimal :amount_received

      t.timestamps
    end
  end
end
