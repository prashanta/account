class CreateInventoryOuts < ActiveRecord::Migration
  def change
    create_table :inventory_outs do |t|
      t.references :item, index: true
      t.float :quantity
      t.float :rate
      t.decimal :amount
      t.integer :commercial_document_id
      t.string :commercial_document_type
      t.date :out_date

      t.timestamps
    end
  end
end
