class RemoveDescriptionFromBillAccounts < ActiveRecord::Migration
  def change
    remove_column :bill_accounts, :description, :string
  end
end
