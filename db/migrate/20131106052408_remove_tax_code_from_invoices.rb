class RemoveTaxCodeFromInvoices < ActiveRecord::Migration
  def change
    remove_reference :invoices, :tax_code, index: true
    remove_column :invoices, :tax_amount, :float
  end
end
