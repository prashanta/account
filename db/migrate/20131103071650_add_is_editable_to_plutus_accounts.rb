class AddIsEditableToPlutusAccounts < ActiveRecord::Migration
  def change
    add_column :plutus_accounts, :is_editable, :boolean
  end
end
