class AddTaxCodeToInvoiceItems < ActiveRecord::Migration
  def change
    add_reference :invoice_items, :tax_code, index: true
    add_column :invoice_items, :tax_amount, :float
  end
end
