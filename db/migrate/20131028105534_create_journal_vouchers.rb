class CreateJournalVouchers < ActiveRecord::Migration
  def change
    create_table :journal_vouchers do |t|
      t.date :voucher_date
      t.string :reference_no
      t.string :narration
      t.text :notes

      t.timestamps
    end
  end
end
