class CreateReturnInvoiceItems < ActiveRecord::Migration
  def change
    create_table :return_invoice_items do |t|
      t.references :return_invoice, index: true
      t.references :invoice_item, index: true
      t.references :tax_code, index: true
      t.float :quantity
      t.float :rate
      t.decimal :amount
      t.decimal :tax_amount

      t.timestamps
    end
  end
end
