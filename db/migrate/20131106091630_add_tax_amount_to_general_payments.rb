class AddTaxAmountToGeneralPayments < ActiveRecord::Migration
  def change
    add_column :general_payments, :tax_amount, :float
  end
end
