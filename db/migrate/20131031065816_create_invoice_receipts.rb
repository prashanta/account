class CreateInvoiceReceipts < ActiveRecord::Migration
  def change
    create_table :invoice_receipts do |t|
      t.references :invoice, index: true
      t.decimal :amount
      t.references :invoice_receipt_voucher, index: true

      t.timestamps
    end
  end
end
