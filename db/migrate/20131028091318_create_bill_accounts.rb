class CreateBillAccounts < ActiveRecord::Migration
  def change
    create_table :bill_accounts do |t|
      t.string :description
      t.references :account, index: true
      t.decimal :amount

      t.timestamps
    end
  end
end
