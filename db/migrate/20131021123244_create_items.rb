class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :unit
      t.text :description
      t.references :account, index: true

      t.timestamps
    end
  end
end
