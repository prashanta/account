class CreateProductItems < ActiveRecord::Migration
  def change
    create_table :product_items do |t|
      t.references :item, index: true
      t.references :production, index: true
      t.float :qty
      t.float :mrp
      t.float :rate
      t.decimal :amount

      t.timestamps
    end
  end
end
