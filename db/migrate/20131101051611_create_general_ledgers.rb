class CreateGeneralLedgers < ActiveRecord::Migration
  def change
    create_table :general_ledgers do |t|
      t.date :from
      t.date :to

      t.timestamps
    end
  end
end
