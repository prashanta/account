class RemoveAccountFromItem < ActiveRecord::Migration
  def change
    remove_reference :items, :account, index: true
  end
end
