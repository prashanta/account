class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name
      t.text :address
      t.string :email
      t.string :telephone
      t.string :mobile
      t.text :additional_information

      t.timestamps
    end
  end
end
