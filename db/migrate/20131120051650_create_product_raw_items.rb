class CreateProductRawItems < ActiveRecord::Migration
  def change
    create_table :product_raw_items do |t|
      t.references :item, index: true
      t.references :production, index: true
      t.float :qty

      t.timestamps
    end
  end
end
