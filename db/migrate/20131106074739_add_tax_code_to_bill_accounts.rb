class AddTaxCodeToBillAccounts < ActiveRecord::Migration
  def change
    add_reference :bill_accounts, :tax_code, index: true
    add_column :bill_accounts, :tax_amount, :float
    add_reference :bill_accounts, :item, index: true
    add_column :bill_accounts, :qty, :float
    add_column :bill_accounts, :price, :float
  end
end
