class CreateProductions < ActiveRecord::Migration
  def change
    create_table :productions do |t|
      t.string :batch_no
      t.date :production_date
      t.boolean :approved
      t.text :internal_information

      t.timestamps
    end
  end
end
