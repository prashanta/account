class RemoveTaxCodeFromBills < ActiveRecord::Migration
  def change
    remove_reference :bills, :tax_code, index: true
    remove_column :bills, :tax_amount, :float
  end
end
