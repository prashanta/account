class CreateBalanceSheets < ActiveRecord::Migration
  def change
    create_table :balance_sheets do |t|
      t.date :as_at

      t.timestamps
    end
  end
end
