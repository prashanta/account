class CreateReturnInvoices < ActiveRecord::Migration
  def change
    create_table :return_invoices do |t|
      t.references :invoice, index: true
      t.decimal :amount
      t.date :return_date
      t.string :reference_no
      t.text :description

      t.timestamps
    end
  end
end
