class AddTaxAmountToGeneralReceipts < ActiveRecord::Migration
  def change
    add_column :general_receipts, :tax_amount, :float
  end
end
