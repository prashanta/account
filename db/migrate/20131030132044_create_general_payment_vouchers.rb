class CreateGeneralPaymentVouchers < ActiveRecord::Migration
  def change
    create_table :general_payment_vouchers do |t|
      t.string :reference_no
      t.date :payment_date
      t.string :payee
      t.references :account, index: true
      t.text :description

      t.timestamps
    end
  end
end
