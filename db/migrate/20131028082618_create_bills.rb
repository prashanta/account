class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.date :issue_date
      t.date :due_date
      t.references :supplier, index: true
      t.string :reference_no
      t.text :internal_information
      t.references :tax_code, index: true
      t.float :tax_amount
      t.decimal :amount
      t.decimal :amount_paid

      t.timestamps
    end
  end
end
