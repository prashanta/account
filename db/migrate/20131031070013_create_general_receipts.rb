class CreateGeneralReceipts < ActiveRecord::Migration
  def change
    create_table :general_receipts do |t|
      t.references :account, index: true
      t.decimal :amount
      t.references :general_receipt_voucher, index: true
      t.references :tax_code, index: true

      t.timestamps
    end
  end
end
