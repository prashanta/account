class RemoveRateFromProductItems < ActiveRecord::Migration
  def change
    remove_column :product_items, :rate, :float
    remove_column :product_items, :amount, :decimal
  end
end
