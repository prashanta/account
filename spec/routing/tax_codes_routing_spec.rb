require "spec_helper"

describe TaxCodesController do
  describe "routing" do

    it "routes to #index" do
      get("/tax_codes").should route_to("tax_codes#index")
    end

    it "routes to #new" do
      get("/tax_codes/new").should route_to("tax_codes#new")
    end

    it "routes to #show" do
      get("/tax_codes/1").should route_to("tax_codes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/tax_codes/1/edit").should route_to("tax_codes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/tax_codes").should route_to("tax_codes#create")
    end

    it "routes to #update" do
      put("/tax_codes/1").should route_to("tax_codes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/tax_codes/1").should route_to("tax_codes#destroy", :id => "1")
    end

  end
end
