require "spec_helper"

describe GeneralLedgersController do
  describe "routing" do

    it "routes to #index" do
      get("/general_ledgers").should route_to("general_ledgers#index")
    end

    it "routes to #new" do
      get("/general_ledgers/new").should route_to("general_ledgers#new")
    end

    it "routes to #show" do
      get("/general_ledgers/1").should route_to("general_ledgers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/general_ledgers/1/edit").should route_to("general_ledgers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/general_ledgers").should route_to("general_ledgers#create")
    end

    it "routes to #update" do
      put("/general_ledgers/1").should route_to("general_ledgers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/general_ledgers/1").should route_to("general_ledgers#destroy", :id => "1")
    end

  end
end
