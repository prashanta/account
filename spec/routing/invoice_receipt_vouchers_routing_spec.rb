require "spec_helper"

describe InvoiceReceiptVouchersController do
  describe "routing" do

    it "routes to #index" do
      get("/invoice_receipt_vouchers").should route_to("invoice_receipt_vouchers#index")
    end

    it "routes to #new" do
      get("/invoice_receipt_vouchers/new").should route_to("invoice_receipt_vouchers#new")
    end

    it "routes to #show" do
      get("/invoice_receipt_vouchers/1").should route_to("invoice_receipt_vouchers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/invoice_receipt_vouchers/1/edit").should route_to("invoice_receipt_vouchers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/invoice_receipt_vouchers").should route_to("invoice_receipt_vouchers#create")
    end

    it "routes to #update" do
      put("/invoice_receipt_vouchers/1").should route_to("invoice_receipt_vouchers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/invoice_receipt_vouchers/1").should route_to("invoice_receipt_vouchers#destroy", :id => "1")
    end

  end
end
