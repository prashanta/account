require "spec_helper"

describe ReturnInvoicesController do
  describe "routing" do

    it "routes to #index" do
      get("/return_invoices").should route_to("return_invoices#index")
    end

    it "routes to #new" do
      get("/return_invoices/new").should route_to("return_invoices#new")
    end

    it "routes to #show" do
      get("/return_invoices/1").should route_to("return_invoices#show", :id => "1")
    end

    it "routes to #edit" do
      get("/return_invoices/1/edit").should route_to("return_invoices#edit", :id => "1")
    end

    it "routes to #create" do
      post("/return_invoices").should route_to("return_invoices#create")
    end

    it "routes to #update" do
      put("/return_invoices/1").should route_to("return_invoices#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/return_invoices/1").should route_to("return_invoices#destroy", :id => "1")
    end

  end
end
