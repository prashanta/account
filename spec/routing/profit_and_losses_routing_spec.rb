require "spec_helper"

describe ProfitAndLossesController do
  describe "routing" do

    it "routes to #index" do
      get("/profit_and_losses").should route_to("profit_and_losses#index")
    end

    it "routes to #new" do
      get("/profit_and_losses/new").should route_to("profit_and_losses#new")
    end

    it "routes to #show" do
      get("/profit_and_losses/1").should route_to("profit_and_losses#show", :id => "1")
    end

    it "routes to #edit" do
      get("/profit_and_losses/1/edit").should route_to("profit_and_losses#edit", :id => "1")
    end

    it "routes to #create" do
      post("/profit_and_losses").should route_to("profit_and_losses#create")
    end

    it "routes to #update" do
      put("/profit_and_losses/1").should route_to("profit_and_losses#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/profit_and_losses/1").should route_to("profit_and_losses#destroy", :id => "1")
    end

  end
end
