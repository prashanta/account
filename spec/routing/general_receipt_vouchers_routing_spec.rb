require "spec_helper"

describe GeneralReceiptVouchersController do
  describe "routing" do

    it "routes to #index" do
      get("/general_receipt_vouchers").should route_to("general_receipt_vouchers#index")
    end

    it "routes to #new" do
      get("/general_receipt_vouchers/new").should route_to("general_receipt_vouchers#new")
    end

    it "routes to #show" do
      get("/general_receipt_vouchers/1").should route_to("general_receipt_vouchers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/general_receipt_vouchers/1/edit").should route_to("general_receipt_vouchers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/general_receipt_vouchers").should route_to("general_receipt_vouchers#create")
    end

    it "routes to #update" do
      put("/general_receipt_vouchers/1").should route_to("general_receipt_vouchers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/general_receipt_vouchers/1").should route_to("general_receipt_vouchers#destroy", :id => "1")
    end

  end
end
