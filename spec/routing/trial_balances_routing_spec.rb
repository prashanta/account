require "spec_helper"

describe TrialBalancesController do
  describe "routing" do

    it "routes to #index" do
      get("/trial_balances").should route_to("trial_balances#index")
    end

    it "routes to #new" do
      get("/trial_balances/new").should route_to("trial_balances#new")
    end

    it "routes to #show" do
      get("/trial_balances/1").should route_to("trial_balances#show", :id => "1")
    end

    it "routes to #edit" do
      get("/trial_balances/1/edit").should route_to("trial_balances#edit", :id => "1")
    end

    it "routes to #create" do
      post("/trial_balances").should route_to("trial_balances#create")
    end

    it "routes to #update" do
      put("/trial_balances/1").should route_to("trial_balances#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/trial_balances/1").should route_to("trial_balances#destroy", :id => "1")
    end

  end
end
