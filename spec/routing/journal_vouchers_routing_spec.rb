require "spec_helper"

describe JournalVouchersController do
  describe "routing" do

    it "routes to #index" do
      get("/journal_vouchers").should route_to("journal_vouchers#index")
    end

    it "routes to #new" do
      get("/journal_vouchers/new").should route_to("journal_vouchers#new")
    end

    it "routes to #show" do
      get("/journal_vouchers/1").should route_to("journal_vouchers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/journal_vouchers/1/edit").should route_to("journal_vouchers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/journal_vouchers").should route_to("journal_vouchers#create")
    end

    it "routes to #update" do
      put("/journal_vouchers/1").should route_to("journal_vouchers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/journal_vouchers/1").should route_to("journal_vouchers#destroy", :id => "1")
    end

  end
end
