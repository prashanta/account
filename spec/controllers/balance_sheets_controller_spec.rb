require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe BalanceSheetsController do

  # This should return the minimal set of attributes required to create a valid
  # BalanceSheet. As you add validations to BalanceSheet, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { { "as_at" => "2013-11-01" } }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # BalanceSheetsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET index" do
    it "assigns all balance_sheets as @balance_sheets" do
      balance_sheet = BalanceSheet.create! valid_attributes
      get :index, {}, valid_session
      assigns(:balance_sheets).should eq([balance_sheet])
    end
  end

  describe "GET show" do
    it "assigns the requested balance_sheet as @balance_sheet" do
      balance_sheet = BalanceSheet.create! valid_attributes
      get :show, {:id => balance_sheet.to_param}, valid_session
      assigns(:balance_sheet).should eq(balance_sheet)
    end
  end

  describe "GET new" do
    it "assigns a new balance_sheet as @balance_sheet" do
      get :new, {}, valid_session
      assigns(:balance_sheet).should be_a_new(BalanceSheet)
    end
  end

  describe "GET edit" do
    it "assigns the requested balance_sheet as @balance_sheet" do
      balance_sheet = BalanceSheet.create! valid_attributes
      get :edit, {:id => balance_sheet.to_param}, valid_session
      assigns(:balance_sheet).should eq(balance_sheet)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new BalanceSheet" do
        expect {
          post :create, {:balance_sheet => valid_attributes}, valid_session
        }.to change(BalanceSheet, :count).by(1)
      end

      it "assigns a newly created balance_sheet as @balance_sheet" do
        post :create, {:balance_sheet => valid_attributes}, valid_session
        assigns(:balance_sheet).should be_a(BalanceSheet)
        assigns(:balance_sheet).should be_persisted
      end

      it "redirects to the created balance_sheet" do
        post :create, {:balance_sheet => valid_attributes}, valid_session
        response.should redirect_to(BalanceSheet.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved balance_sheet as @balance_sheet" do
        # Trigger the behavior that occurs when invalid params are submitted
        BalanceSheet.any_instance.stub(:save).and_return(false)
        post :create, {:balance_sheet => { "as_at" => "invalid value" }}, valid_session
        assigns(:balance_sheet).should be_a_new(BalanceSheet)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        BalanceSheet.any_instance.stub(:save).and_return(false)
        post :create, {:balance_sheet => { "as_at" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested balance_sheet" do
        balance_sheet = BalanceSheet.create! valid_attributes
        # Assuming there are no other balance_sheets in the database, this
        # specifies that the BalanceSheet created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        BalanceSheet.any_instance.should_receive(:update).with({ "as_at" => "2013-11-01" })
        put :update, {:id => balance_sheet.to_param, :balance_sheet => { "as_at" => "2013-11-01" }}, valid_session
      end

      it "assigns the requested balance_sheet as @balance_sheet" do
        balance_sheet = BalanceSheet.create! valid_attributes
        put :update, {:id => balance_sheet.to_param, :balance_sheet => valid_attributes}, valid_session
        assigns(:balance_sheet).should eq(balance_sheet)
      end

      it "redirects to the balance_sheet" do
        balance_sheet = BalanceSheet.create! valid_attributes
        put :update, {:id => balance_sheet.to_param, :balance_sheet => valid_attributes}, valid_session
        response.should redirect_to(balance_sheet)
      end
    end

    describe "with invalid params" do
      it "assigns the balance_sheet as @balance_sheet" do
        balance_sheet = BalanceSheet.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        BalanceSheet.any_instance.stub(:save).and_return(false)
        put :update, {:id => balance_sheet.to_param, :balance_sheet => { "as_at" => "invalid value" }}, valid_session
        assigns(:balance_sheet).should eq(balance_sheet)
      end

      it "re-renders the 'edit' template" do
        balance_sheet = BalanceSheet.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        BalanceSheet.any_instance.stub(:save).and_return(false)
        put :update, {:id => balance_sheet.to_param, :balance_sheet => { "as_at" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested balance_sheet" do
      balance_sheet = BalanceSheet.create! valid_attributes
      expect {
        delete :destroy, {:id => balance_sheet.to_param}, valid_session
      }.to change(BalanceSheet, :count).by(-1)
    end

    it "redirects to the balance_sheets list" do
      balance_sheet = BalanceSheet.create! valid_attributes
      delete :destroy, {:id => balance_sheet.to_param}, valid_session
      response.should redirect_to(balance_sheets_url)
    end
  end

end
