# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :general_payment_voucher do
    reference_no "MyString"
    payment_date "2013-10-30"
    payee "MyString"
    account nil
    description "MyText"
  end
end
