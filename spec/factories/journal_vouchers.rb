# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :journal_voucher do
    voucher_date "2013-10-28"
    reference_no "MyString"
    narration "MyString"
    notes "MyText"
  end
end
