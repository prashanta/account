# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tax_code do
    name "MyString"
    rate 1.5
  end
end
