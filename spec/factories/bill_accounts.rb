# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill_account do
    description "MyString"
    account nil
    amount "9.99"
  end
end
