# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice do
    invoice_no "MyString"
    issue_date "2013-10-22"
    due_date "2013-10-22"
    customer nil
    internal_information "MyText"
    tax_code nil
    tax_amount 1
    amount "9.99"
    amount_received "9.99"
  end
end
