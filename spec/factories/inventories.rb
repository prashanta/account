# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inventory do
    item nil
    quantity 1.5
    rate 1.5
  end
end
