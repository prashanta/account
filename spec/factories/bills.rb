# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill do
    issue_date "2013-10-28"
    due_date "2013-10-28"
    supplier nil
    reference_no "MyString"
    internal_information "MyText"
    tax_code nil
    tax_amount ""
    amount ""
    amount_paid "9.99"
  end
end
