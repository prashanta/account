# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice_item do
    item nil
    description "MyText"
    account nil
    qty 1.5
    price 1.5
    amount "9.99"
  end
end
