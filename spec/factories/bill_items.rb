# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill_item do
    account nil
    bill nil
    tax_code nil
    tax_amount 1.5
    item nil
    qty 1.5
    price 1.5
    amount "9.99"
  end
end
