# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :profit_and_loss do
    from "2013-11-01"
    to "2013-11-01"
  end
end
