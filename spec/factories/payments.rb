# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    account nil
    amount "9.99"
    tax_code nil
  end
end
