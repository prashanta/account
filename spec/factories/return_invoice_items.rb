# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :return_invoice_item do
    return_invoice nil
    invoice_item nil
    quantity 1.5
    rate 1.5
    amount "9.99"
    tax_amount "9.99"
  end
end
