# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice_receipt do
    invoice nil
    amount "9.99"
    invoice_receipt_voucher nil
  end
end
