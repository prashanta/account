# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :supplier do
    name "MyString"
    address "MyText"
    email "MyString"
    telephone "MyString"
    mobile "MyString"
    additional_information "MyText"
  end
end
