# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :general_receipt_voucher do
    reference_no "MyString"
    payment_date "2013-10-31"
    payer "MyString"
    account nil
    description "MyText"
  end
end
