# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :return_invoice do
    invoice nil
    amount "9.99"
    return_date "2013-11-25"
    reference_no "MyString"
    description "MyText"
  end
end
