# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    item nil
    production nil
    qty 1.5
    mrp 1.5
    rate 1.5
    amount "9.99"
  end
end
