# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trial_balance do
    from "2013-10-31"
    to "2013-10-31"
  end
end
