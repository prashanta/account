# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :general_payment do
    account nil
    amount "9.99"
    general_payment_voucher nil
    tax_code nil
  end
end
