# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :balance_sheet do
    as_at "2013-11-01"
  end
end
