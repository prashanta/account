# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill_payment do
    bill nil
    amount "9.99"
    bill_payment_voucher nil
  end
end
