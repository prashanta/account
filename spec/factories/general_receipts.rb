# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :general_receipt do
    account nil
    amount "9.99"
    general_receipt_voucher nil
    tax_code nil
  end
end
