# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :production do
    batch_no "MyString"
    production_date "2013-11-20"
    approved false
    internal_information "MyText"
  end
end
