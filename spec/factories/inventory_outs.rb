# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inventory_out do
    item nil
    quantity 1.5
    rate 1.5
    amount "9.99"
    commercial_document_id 1
    commercial_document_type "MyString"
    out_date "2013-11-19"
  end
end
