# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :credit_journal do
    journal_voucher nil
    account nil
    description "MyString"
    amount "9.99"
  end
end
