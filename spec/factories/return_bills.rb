# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :return_bill do
    bill nil
    amount "9.99"
    bill_return_voucher nil
  end
end
