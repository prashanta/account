require 'spec_helper'

describe "suppliers/show" do
  before(:each) do
    @supplier = assign(:supplier, stub_model(Supplier,
      :name => "Name",
      :address => "MyText",
      :email => "Email",
      :telephone => "Telephone",
      :mobile => "Mobile",
      :additional_information => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/Email/)
    rendered.should match(/Telephone/)
    rendered.should match(/Mobile/)
    rendered.should match(/MyText/)
  end
end
