require 'spec_helper'

describe "suppliers/new" do
  before(:each) do
    assign(:supplier, stub_model(Supplier,
      :name => "MyString",
      :address => "MyText",
      :email => "MyString",
      :telephone => "MyString",
      :mobile => "MyString",
      :additional_information => "MyText"
    ).as_new_record)
  end

  it "renders new supplier form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", suppliers_path, "post" do
      assert_select "input#supplier_name[name=?]", "supplier[name]"
      assert_select "textarea#supplier_address[name=?]", "supplier[address]"
      assert_select "input#supplier_email[name=?]", "supplier[email]"
      assert_select "input#supplier_telephone[name=?]", "supplier[telephone]"
      assert_select "input#supplier_mobile[name=?]", "supplier[mobile]"
      assert_select "textarea#supplier_additional_information[name=?]", "supplier[additional_information]"
    end
  end
end
