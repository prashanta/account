require 'spec_helper'

describe "general_payment_vouchers/index" do
  before(:each) do
    assign(:general_payment_vouchers, [
      stub_model(GeneralPaymentVoucher,
        :reference_no => "Reference No",
        :payee => "Payee",
        :account => nil,
        :description => "MyText"
      ),
      stub_model(GeneralPaymentVoucher,
        :reference_no => "Reference No",
        :payee => "Payee",
        :account => nil,
        :description => "MyText"
      )
    ])
  end

  it "renders a list of general_payment_vouchers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Reference No".to_s, :count => 2
    assert_select "tr>td", :text => "Payee".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
