require 'spec_helper'

describe "general_payment_vouchers/new" do
  before(:each) do
    assign(:general_payment_voucher, stub_model(GeneralPaymentVoucher,
      :reference_no => "MyString",
      :payee => "MyString",
      :account => nil,
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new general_payment_voucher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", general_payment_vouchers_path, "post" do
      assert_select "input#general_payment_voucher_reference_no[name=?]", "general_payment_voucher[reference_no]"
      assert_select "input#general_payment_voucher_payee[name=?]", "general_payment_voucher[payee]"
      assert_select "input#general_payment_voucher_account[name=?]", "general_payment_voucher[account]"
      assert_select "textarea#general_payment_voucher_description[name=?]", "general_payment_voucher[description]"
    end
  end
end
