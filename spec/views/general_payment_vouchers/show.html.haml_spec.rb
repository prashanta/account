require 'spec_helper'

describe "general_payment_vouchers/show" do
  before(:each) do
    @general_payment_voucher = assign(:general_payment_voucher, stub_model(GeneralPaymentVoucher,
      :reference_no => "Reference No",
      :payee => "Payee",
      :account => nil,
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Reference No/)
    rendered.should match(/Payee/)
    rendered.should match(//)
    rendered.should match(/MyText/)
  end
end
