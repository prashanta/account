require 'spec_helper'

describe "trial_balances/new" do
  before(:each) do
    assign(:trial_balance, stub_model(TrialBalance).as_new_record)
  end

  it "renders new trial_balance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", trial_balances_path, "post" do
    end
  end
end
