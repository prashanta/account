require 'spec_helper'

describe "trial_balances/edit" do
  before(:each) do
    @trial_balance = assign(:trial_balance, stub_model(TrialBalance))
  end

  it "renders the edit trial_balance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", trial_balance_path(@trial_balance), "post" do
    end
  end
end
