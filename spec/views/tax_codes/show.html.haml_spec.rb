require 'spec_helper'

describe "tax_codes/show" do
  before(:each) do
    @tax_code = assign(:tax_code, stub_model(TaxCode,
      :name => "Name",
      :rate => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/1.5/)
  end
end
