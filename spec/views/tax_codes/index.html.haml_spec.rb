require 'spec_helper'

describe "tax_codes/index" do
  before(:each) do
    assign(:tax_codes, [
      stub_model(TaxCode,
        :name => "Name",
        :rate => 1.5
      ),
      stub_model(TaxCode,
        :name => "Name",
        :rate => 1.5
      )
    ])
  end

  it "renders a list of tax_codes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
