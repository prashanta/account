require 'spec_helper'

describe "tax_codes/edit" do
  before(:each) do
    @tax_code = assign(:tax_code, stub_model(TaxCode,
      :name => "MyString",
      :rate => 1.5
    ))
  end

  it "renders the edit tax_code form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", tax_code_path(@tax_code), "post" do
      assert_select "input#tax_code_name[name=?]", "tax_code[name]"
      assert_select "input#tax_code_rate[name=?]", "tax_code[rate]"
    end
  end
end
