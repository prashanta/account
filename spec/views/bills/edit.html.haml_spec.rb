require 'spec_helper'

describe "bills/edit" do
  before(:each) do
    @bill = assign(:bill, stub_model(Bill,
      :supplier => nil,
      :reference_no => "MyString",
      :internal_information => "MyText",
      :tax_code => nil,
      :tax_amount => "",
      :amount => "",
      :amount_paid => "9.99"
    ))
  end

  it "renders the edit bill form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", bill_path(@bill), "post" do
      assert_select "input#bill_supplier[name=?]", "bill[supplier]"
      assert_select "input#bill_reference_no[name=?]", "bill[reference_no]"
      assert_select "textarea#bill_internal_information[name=?]", "bill[internal_information]"
      assert_select "input#bill_tax_code[name=?]", "bill[tax_code]"
      assert_select "input#bill_tax_amount[name=?]", "bill[tax_amount]"
      assert_select "input#bill_amount[name=?]", "bill[amount]"
      assert_select "input#bill_amount_paid[name=?]", "bill[amount_paid]"
    end
  end
end
