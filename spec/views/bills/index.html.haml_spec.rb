require 'spec_helper'

describe "bills/index" do
  before(:each) do
    assign(:bills, [
      stub_model(Bill,
        :supplier => nil,
        :reference_no => "Reference No",
        :internal_information => "MyText",
        :tax_code => nil,
        :tax_amount => "",
        :amount => "",
        :amount_paid => "9.99"
      ),
      stub_model(Bill,
        :supplier => nil,
        :reference_no => "Reference No",
        :internal_information => "MyText",
        :tax_code => nil,
        :tax_amount => "",
        :amount => "",
        :amount_paid => "9.99"
      )
    ])
  end

  it "renders a list of bills" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Reference No".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
