require 'spec_helper'

describe "invoice_receipt_vouchers/edit" do
  before(:each) do
    @invoice_receipt_voucher = assign(:invoice_receipt_voucher, stub_model(InvoiceReceiptVoucher,
      :reference_no => "MyString",
      :payer => "MyString",
      :account => nil,
      :description => "MyText"
    ))
  end

  it "renders the edit invoice_receipt_voucher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", invoice_receipt_voucher_path(@invoice_receipt_voucher), "post" do
      assert_select "input#invoice_receipt_voucher_reference_no[name=?]", "invoice_receipt_voucher[reference_no]"
      assert_select "input#invoice_receipt_voucher_payer[name=?]", "invoice_receipt_voucher[payer]"
      assert_select "input#invoice_receipt_voucher_account[name=?]", "invoice_receipt_voucher[account]"
      assert_select "textarea#invoice_receipt_voucher_description[name=?]", "invoice_receipt_voucher[description]"
    end
  end
end
