require 'spec_helper'

describe "invoice_receipt_vouchers/show" do
  before(:each) do
    @invoice_receipt_voucher = assign(:invoice_receipt_voucher, stub_model(InvoiceReceiptVoucher,
      :reference_no => "Reference No",
      :payer => "Payer",
      :account => nil,
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Reference No/)
    rendered.should match(/Payer/)
    rendered.should match(//)
    rendered.should match(/MyText/)
  end
end
