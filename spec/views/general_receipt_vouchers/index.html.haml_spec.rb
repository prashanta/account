require 'spec_helper'

describe "general_receipt_vouchers/index" do
  before(:each) do
    assign(:general_receipt_vouchers, [
      stub_model(GeneralReceiptVoucher,
        :reference_no => "Reference No",
        :payer => "Payer",
        :account => nil,
        :description => "MyText"
      ),
      stub_model(GeneralReceiptVoucher,
        :reference_no => "Reference No",
        :payer => "Payer",
        :account => nil,
        :description => "MyText"
      )
    ])
  end

  it "renders a list of general_receipt_vouchers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Reference No".to_s, :count => 2
    assert_select "tr>td", :text => "Payer".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
