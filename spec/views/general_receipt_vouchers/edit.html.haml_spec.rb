require 'spec_helper'

describe "general_receipt_vouchers/edit" do
  before(:each) do
    @general_receipt_voucher = assign(:general_receipt_voucher, stub_model(GeneralReceiptVoucher,
      :reference_no => "MyString",
      :payer => "MyString",
      :account => nil,
      :description => "MyText"
    ))
  end

  it "renders the edit general_receipt_voucher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", general_receipt_voucher_path(@general_receipt_voucher), "post" do
      assert_select "input#general_receipt_voucher_reference_no[name=?]", "general_receipt_voucher[reference_no]"
      assert_select "input#general_receipt_voucher_payer[name=?]", "general_receipt_voucher[payer]"
      assert_select "input#general_receipt_voucher_account[name=?]", "general_receipt_voucher[account]"
      assert_select "textarea#general_receipt_voucher_description[name=?]", "general_receipt_voucher[description]"
    end
  end
end
