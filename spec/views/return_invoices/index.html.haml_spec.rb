require 'spec_helper'

describe "return_invoices/index" do
  before(:each) do
    assign(:return_invoices, [
      stub_model(ReturnInvoice,
        :invoice => nil,
        :amount => "9.99",
        :reference_no => "Reference No",
        :description => "MyText"
      ),
      stub_model(ReturnInvoice,
        :invoice => nil,
        :amount => "9.99",
        :reference_no => "Reference No",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of return_invoices" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Reference No".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
