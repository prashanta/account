require 'spec_helper'

describe "return_invoices/show" do
  before(:each) do
    @return_invoice = assign(:return_invoice, stub_model(ReturnInvoice,
      :invoice => nil,
      :amount => "9.99",
      :reference_no => "Reference No",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/9.99/)
    rendered.should match(/Reference No/)
    rendered.should match(/MyText/)
  end
end
