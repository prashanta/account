require 'spec_helper'

describe "return_invoices/new" do
  before(:each) do
    assign(:return_invoice, stub_model(ReturnInvoice,
      :invoice => nil,
      :amount => "9.99",
      :reference_no => "MyString",
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new return_invoice form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", return_invoices_path, "post" do
      assert_select "input#return_invoice_invoice[name=?]", "return_invoice[invoice]"
      assert_select "input#return_invoice_amount[name=?]", "return_invoice[amount]"
      assert_select "input#return_invoice_reference_no[name=?]", "return_invoice[reference_no]"
      assert_select "textarea#return_invoice_description[name=?]", "return_invoice[description]"
    end
  end
end
