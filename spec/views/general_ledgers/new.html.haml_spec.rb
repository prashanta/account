require 'spec_helper'

describe "general_ledgers/new" do
  before(:each) do
    assign(:general_ledger, stub_model(GeneralLedger).as_new_record)
  end

  it "renders new general_ledger form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", general_ledgers_path, "post" do
    end
  end
end
