require 'spec_helper'

describe "general_ledgers/edit" do
  before(:each) do
    @general_ledger = assign(:general_ledger, stub_model(GeneralLedger))
  end

  it "renders the edit general_ledger form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", general_ledger_path(@general_ledger), "post" do
    end
  end
end
