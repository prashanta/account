require 'spec_helper'

describe "customers/index" do
  before(:each) do
    assign(:customers, [
      stub_model(Customer,
        :name => "Name",
        :address => "MyText",
        :email => "Email",
        :telephone => "Telephone",
        :mobile => "Mobile",
        :additional_information => "MyText"
      ),
      stub_model(Customer,
        :name => "Name",
        :address => "MyText",
        :email => "Email",
        :telephone => "Telephone",
        :mobile => "Mobile",
        :additional_information => "MyText"
      )
    ])
  end

  it "renders a list of customers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Telephone".to_s, :count => 2
    assert_select "tr>td", :text => "Mobile".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
