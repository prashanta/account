require 'spec_helper'

describe "customers/edit" do
  before(:each) do
    @customer = assign(:customer, stub_model(Customer,
      :name => "MyString",
      :address => "MyText",
      :email => "MyString",
      :telephone => "MyString",
      :mobile => "MyString",
      :additional_information => "MyText"
    ))
  end

  it "renders the edit customer form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", customer_path(@customer), "post" do
      assert_select "input#customer_name[name=?]", "customer[name]"
      assert_select "textarea#customer_address[name=?]", "customer[address]"
      assert_select "input#customer_email[name=?]", "customer[email]"
      assert_select "input#customer_telephone[name=?]", "customer[telephone]"
      assert_select "input#customer_mobile[name=?]", "customer[mobile]"
      assert_select "textarea#customer_additional_information[name=?]", "customer[additional_information]"
    end
  end
end
