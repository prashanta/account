require 'spec_helper'

describe "customers/show" do
  before(:each) do
    @customer = assign(:customer, stub_model(Customer,
      :name => "Name",
      :address => "MyText",
      :email => "Email",
      :telephone => "Telephone",
      :mobile => "Mobile",
      :additional_information => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
    rendered.should match(/Email/)
    rendered.should match(/Telephone/)
    rendered.should match(/Mobile/)
    rendered.should match(/MyText/)
  end
end
