require 'spec_helper'

describe "bill_payment_vouchers/edit" do
  before(:each) do
    @bill_payment_voucher = assign(:bill_payment_voucher, stub_model(BillPaymentVoucher,
      :reference_no => "MyString",
      :payee => "MyString",
      :account => nil,
      :description => "MyText"
    ))
  end

  it "renders the edit bill_payment_voucher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", bill_payment_voucher_path(@bill_payment_voucher), "post" do
      assert_select "input#bill_payment_voucher_reference_no[name=?]", "bill_payment_voucher[reference_no]"
      assert_select "input#bill_payment_voucher_payee[name=?]", "bill_payment_voucher[payee]"
      assert_select "input#bill_payment_voucher_account[name=?]", "bill_payment_voucher[account]"
      assert_select "textarea#bill_payment_voucher_description[name=?]", "bill_payment_voucher[description]"
    end
  end
end
