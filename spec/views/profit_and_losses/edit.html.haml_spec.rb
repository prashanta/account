require 'spec_helper'

describe "profit_and_losses/edit" do
  before(:each) do
    @profit_and_loss = assign(:profit_and_loss, stub_model(ProfitAndLoss))
  end

  it "renders the edit profit_and_loss form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", profit_and_loss_path(@profit_and_loss), "post" do
    end
  end
end
