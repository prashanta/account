require 'spec_helper'

describe "profit_and_losses/new" do
  before(:each) do
    assign(:profit_and_loss, stub_model(ProfitAndLoss).as_new_record)
  end

  it "renders new profit_and_loss form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", profit_and_losses_path, "post" do
    end
  end
end
