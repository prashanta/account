require 'spec_helper'

describe "productions/show" do
  before(:each) do
    @production = assign(:production, stub_model(Production,
      :batch_no => "Batch No",
      :approved => false,
      :internal_information => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Batch No/)
    rendered.should match(/false/)
    rendered.should match(/MyText/)
  end
end
