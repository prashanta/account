require 'spec_helper'

describe "productions/new" do
  before(:each) do
    assign(:production, stub_model(Production,
      :batch_no => "MyString",
      :approved => false,
      :internal_information => "MyText"
    ).as_new_record)
  end

  it "renders new production form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", productions_path, "post" do
      assert_select "input#production_batch_no[name=?]", "production[batch_no]"
      assert_select "input#production_approved[name=?]", "production[approved]"
      assert_select "textarea#production_internal_information[name=?]", "production[internal_information]"
    end
  end
end
