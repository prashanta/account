require 'spec_helper'

describe "invoices/edit" do
  before(:each) do
    @invoice = assign(:invoice, stub_model(Invoice,
      :invoice_no => "MyString",
      :customer => nil,
      :internal_information => "MyText",
      :tax_code => nil,
      :tax_amount => 1,
      :amount => "9.99",
      :amount_received => "9.99"
    ))
  end

  it "renders the edit invoice form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", invoice_path(@invoice), "post" do
      assert_select "input#invoice_invoice_no[name=?]", "invoice[invoice_no]"
      assert_select "input#invoice_customer[name=?]", "invoice[customer]"
      assert_select "textarea#invoice_internal_information[name=?]", "invoice[internal_information]"
      assert_select "input#invoice_tax_code[name=?]", "invoice[tax_code]"
      assert_select "input#invoice_tax_amount[name=?]", "invoice[tax_amount]"
      assert_select "input#invoice_amount[name=?]", "invoice[amount]"
      assert_select "input#invoice_amount_received[name=?]", "invoice[amount_received]"
    end
  end
end
