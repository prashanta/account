require 'spec_helper'

describe "invoices/index" do
  before(:each) do
    assign(:invoices, [
      stub_model(Invoice,
        :invoice_no => "Invoice No",
        :customer => nil,
        :internal_information => "MyText",
        :tax_code => nil,
        :tax_amount => 1,
        :amount => "9.99",
        :amount_received => "9.99"
      ),
      stub_model(Invoice,
        :invoice_no => "Invoice No",
        :customer => nil,
        :internal_information => "MyText",
        :tax_code => nil,
        :tax_amount => 1,
        :amount => "9.99",
        :amount_received => "9.99"
      )
    ])
  end

  it "renders a list of invoices" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Invoice No".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
