require 'spec_helper'

describe "invoices/show" do
  before(:each) do
    @invoice = assign(:invoice, stub_model(Invoice,
      :invoice_no => "Invoice No",
      :customer => nil,
      :internal_information => "MyText",
      :tax_code => nil,
      :tax_amount => 1,
      :amount => "9.99",
      :amount_received => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Invoice No/)
    rendered.should match(//)
    rendered.should match(/MyText/)
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/9.99/)
    rendered.should match(/9.99/)
  end
end
