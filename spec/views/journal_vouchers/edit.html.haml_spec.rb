require 'spec_helper'

describe "journal_vouchers/edit" do
  before(:each) do
    @journal_voucher = assign(:journal_voucher, stub_model(JournalVoucher,
      :reference_no => "MyString",
      :narration => "MyString",
      :notes => "MyText"
    ))
  end

  it "renders the edit journal_voucher form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", journal_voucher_path(@journal_voucher), "post" do
      assert_select "input#journal_voucher_reference_no[name=?]", "journal_voucher[reference_no]"
      assert_select "input#journal_voucher_narration[name=?]", "journal_voucher[narration]"
      assert_select "textarea#journal_voucher_notes[name=?]", "journal_voucher[notes]"
    end
  end
end
