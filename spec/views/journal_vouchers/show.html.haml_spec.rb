require 'spec_helper'

describe "journal_vouchers/show" do
  before(:each) do
    @journal_voucher = assign(:journal_voucher, stub_model(JournalVoucher,
      :reference_no => "Reference No",
      :narration => "Narration",
      :notes => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Reference No/)
    rendered.should match(/Narration/)
    rendered.should match(/MyText/)
  end
end
