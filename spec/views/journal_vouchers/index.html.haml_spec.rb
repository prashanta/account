require 'spec_helper'

describe "journal_vouchers/index" do
  before(:each) do
    assign(:journal_vouchers, [
      stub_model(JournalVoucher,
        :reference_no => "Reference No",
        :narration => "Narration",
        :notes => "MyText"
      ),
      stub_model(JournalVoucher,
        :reference_no => "Reference No",
        :narration => "Narration",
        :notes => "MyText"
      )
    ])
  end

  it "renders a list of journal_vouchers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Reference No".to_s, :count => 2
    assert_select "tr>td", :text => "Narration".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
