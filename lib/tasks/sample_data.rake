HOURLY_RATE = 123.45
namespace :db do
  desc "Fill database with simple data"
  task :populate => :environment do
   # Rake::Task['db:reset'].invoke

   require 'roo'
   # make_bills
    make_production_note
   # make_invoice
  end
end
#make bills
=begin
  def make_bills   
      oo = Roo::Excelx.new("/home/pmahato/Documents/account_bill.xlsx")
      Plutus::Account
      amount_bb = 0
      oo.default_sheet = oo.sheets.first
      2.upto(oo.last_row) do |line|
      supplier = Supplier.find_by(name: 'RUNGAMATTEE TEA & INDUSTRIES LTD.')
      id           = oo.cell(line,'A')
      bill_no      = oo.cell(line,'B')
      issue_date   = oo.cell(line,'C')
      due_date     = oo.cell(line,'D')
      supplier     = supplier.id
      bill_amount  = oo.cell(line,'K')
      bill_item    = oo.cell(line,'F')
      item_qty     = oo.cell(line,'G')
      amount       = oo.cell(line,'J')
      tax_code = TaxCode.find(oo.cell(line,'L'))
      tax_amount = ((amount*tax_code.rate)/(100+tax_code.rate)).round(2) if !tax_code.blank? and !amount.blank?
      if id.blank?
        bill = Bill.find_by(:reference_no=>bill_no)
      else
        bill = Bill.create!(:issue_date => issue_date, :due_date =>due_date, :supplier_id=>supplier, :reference_no=>bill_no, :internal_information =>"", :amount =>bill_amount)
      end
       if bill
        account = Plutus::Expense.find_by(:name => "Purchase")
        item = Item.find_or_create_by(name: bill_item.upcase,:unit=>'KG', :description=>'Bulk Tea',:item_type=>'ca',:category=>'Tea')
        item.save
        item_in_bill = BillItem.create!(:bill_id=>bill.id,:account_id=>account.id,:item_id=>item.id,:qty=>item_qty,:price=>(amount/item_qty).round(2),:tax_code_id=>tax_code.id, :tax_amount=>tax_amount,:amount=>amount) 
        if item_in_bill.save
          amount_bb += bill_amount.to_f
          puts "\n\n#{item_in_bill.item.name} Amount = #{amount_bb}"
        end
       end
    end
    puts "\n\n#Final Amount = #{amount_bb}"
  end
=end  
#make production notes
  def make_production_note
    oo = Roo::Excelx.new("/home/pmahato/Documents/production_note.xlsx")
    oo.default_sheet = oo.sheets.first
    2.upto(oo.last_row) do |line|
      id             = oo.cell(line,'A')
      batch_no       = oo.cell(line,'B')
      production_dt  = oo.cell(line,'C')
      packing_unit   = oo.cell(line,'D')
      product_item   = oo.cell(line,'E')
      product_type   = oo.cell(line,'F')
      prod_desc      = oo.cell(line,'G')
      prod_qty       = oo.cell(line,'H') 
      mrp            = oo.cell(line,'I')
      raw_item       = oo.cell(line,'J')
      raw_desc       = oo.cell(line,'K')
      raw_qty        = oo.cell(line,'L')
      if id.blank?
        pnote = Production.find_by(:batch_no=>batch_no)
      else
        pnote = Production.create!(:batch_no=>batch_no.to_s, :production_date=>production_dt, :approved=>false, :internal_information=>"Packing Unit:"+packing_unit)
      end

      if pnote.save
        prod_item = Item.find_by(name: product_item.upcase) if !product_item.blank?
        puts "\n\n\n#{product_item.upcase}" if !prod_item.nil?
        if !product_item.blank? and prod_item.nil?
          prod_item = Item.create!(name: product_item.upcase,:unit=>'KG', :description=>product_type,:item_type=>'ca',:category=>'Product') 
          prod_item.save
        end  
        if !product_item.blank? and !prod_qty.blank?
          create_product_item =  ProductItem.create!(:production_id=>pnote.id,:item_id=>prod_item.id,:qty=>prod_qty,:description=>prod_desc,:mrp=>mrp) 
          create_product_item.save
        end
        used_item = Item.find_by(name: raw_item.upcase) if !raw_item.blank?
        puts "\n\n\n#{used_item.name}  Qty=#{raw_qty}"
        if !used_item.nil? and !raw_qty.blank?
          product_raw_item =  ProductRawItem.create!(:production_id=>pnote.id,:item_id=>used_item.id,:qty=>raw_qty,:description=>raw_desc) 
          product_raw_item.save
        end
      end
    end   
 end 
#make invoices
=begin  
  def make_invoice
      oo = Roo::Excelx.new("/home/pmahato/Documents/account_invoice.xlsx")
      Plutus::Account
      oo.default_sheet = oo.sheets.first
      2.upto(oo.last_row) do |line|
        tax_code = TaxCode.find_by(name: 'VAT')
        id         = oo.cell(line,'A')
        invoice_no = oo.cell(line,'B')
        issue_date = oo.cell(line,'C')
        due_date   = oo.cell(line,'D')
        c_name     = oo.cell(line,'E')
        c_address  = oo.cell(line,'F')
        c_email    = oo.cell(line,'G')
        c_contact  = oo.cell(line,'H')
        invoice_item   = oo.cell(line,'I')
        item_category  = oo.cell(line,'J')
        item_desc      = oo.cell(line,'K')
        item_qty    = oo.cell(line,'J')
        item_price  = oo.cell(line,'K')
        item_tax    = oo.cell(line,'L')
        item_amount = oo.cell(line,'M')
        invoice_amount =  oo.cell(line,'N')
        in_info        = oo.cell(line,'O')
        account = Plutus::Expense.find_by(:name => "Sales")
        if id.blank?
          customer = Customer.find_by(name: c_name.upcase)
          invoice  = Invoice.find_by(:invoice_no =>invoice_no)
        else
          customer = Customer.find_or_create_by(:name=>c_name.upcase, :address=>c_address, :email=>c_email, :telephone=>c_contact, :mobile=>'', :additional_information=>'')
          invoice  = Invoice.create!(:invoice_no=>invoice_no, :issue_date=>issue_date, :due_date=>due_date, :customer_id=>customer.id, :internal_information=>in_info, :amount=>invoice_amount, :amount_received=>0.00)
        end
        if invoice.save
          item = Item.find_by(name: invoice_item.upcase)
          InvoiceItem.create!(:invoice_id=>invoice.id,:item_id=>item.id,:account_id=>account.id,:qty=>item_qty,:price=>(item_amount/item_qty).round(2),:tax_code_id=>tax_code.id, :tax_amount=>item_tax,:amount=>item_amount)
        end 
    end
  end
=end